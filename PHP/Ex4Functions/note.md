# Hàm trong php (PHP Functions)
- Sức mạnh thực sự của PHP đến từ các `functions` của nó.
- PHP có hơn 1000 `functions` dựng sẵn và ngoài ra, bạn có thể tạo các `functions` tùy chỉnh của riêng mình.
## Các hàm tích hợp trong PHP
- PHP có hơn 1000 hàm dựng sẵn có thể được gọi trực tiếp, từ bên trong tập lệnh, để thực hiện một tác vụ cụ thể.
*Tài liệu tham khảo PHP chứa các danh mục khác nhau của tất cả các hàm, từ khóa và hằng số PHP, cùng với các ví dụ.*
- [Array](https://www.w3schools.com/php/php_ref_array.asp)
- [Calendar](https://www.w3schools.com/php/php_ref_calendar.asp)
- [Date](https://www.w3schools.com/php/php_ref_date.asp)
- [Directory](https://www.w3schools.com/php/php_ref_directory.asp)
- [Error](https://www.w3schools.com/php/php_ref_error.asp)
- [Exception](https://www.w3schools.com/php/php_ref_exception.asp)
- [Filesystem](https://www.w3schools.com/php/php_ref_filesystem.asp)
- [Filter](https://www.w3schools.com/php/php_ref_filter.asp)
- [FTP](https://www.w3schools.com/php/php_ref_ftp.asp)
- [JSON](https://www.w3schools.com/php/php_ref_json.asp)
- [Keywords](https://www.w3schools.com/php/php_ref_keywords.asp)
- [Libxml](https://www.w3schools.com/php/php_ref_libxml.asp)
- [Mail](https://www.w3schools.com/php/php_ref_mail.asp)
- [Math](https://www.w3schools.com/php/php_ref_math.asp)
- [Misc](https://www.w3schools.com/php/php_ref_misc.asp)
- [MySQLi](https://www.w3schools.com/php/php_ref_mysqli.asp)
- [Network](https://www.w3schools.com/php/php_ref_network.asp)
- [Output Control](https://www.w3schools.com/php/php_ref_output_control.asp)
- [RegEx](https://www.w3schools.com/php/php_ref_regex.asp)
- [SimpleXML](https://www.w3schools.com/php/php_ref_simplexml.asp)
- [Stream](https://www.w3schools.com/php/php_ref_stream.asp)
- [String](https://www.w3schools.com/php/php_ref_string.asp)
- [Variable Handling](https://www.w3schools.com/php/php_ref_variable_handling.asp)
- [XML Parser](https://www.w3schools.com/php/php_ref_xml.asp)
- [Zip](https://www.w3schools.com/php/php_ref_zip.asp)
- [Timezones](https://www.w3schools.com/php/php_ref_timezones.asp)

## Các hàm do người dùng định nghĩa PHP
*Bên cạnh các hàm PHP có sẵn, bạn có thể tạo các hàm của riêng mình.*
- function là một khối các câu lệnh có thể được sử dụng lặp đi lặp lại trong một chương trình.
- Một function sẽ không tự động thực thi khi tải trang.
- Một function sẽ được thực hiện khi được gọi.

## Tạo một hàm do người dùng định nghĩa trong PHP
- Một khai báo hàm do người dùng định nghĩa bắt đầu bằng từ `function`:
**cú pháp**
```php
function functionName() {
//   code to be executed;
}
```
> Lưu ý: Tên hàm phải bắt đầu bằng một chữ cái hoặc dấu gạch dưới. Tên hàm KHÔNG phân biệt chữ hoa chữ thường.
> Tip: Đặt tên cho hàm phản ánh chức năng của hàm đó!

# PHP Function Arguments
- Thông tin có thể được chuyển đến các functions thông qua các arguments (đối số). Một đối số giống như một biến.
- Các đối số được chỉ định sau tên hàm, bên trong dấu ngoặc đơn `()`. Các đối số ngăn cách với nhau bởi dấu `,`

## PHP là một ngôn ngữ được viết 1 cách lỏng lẻo (PHP is a Loosely Typed Language)
- PHP tự động liên kết kiểu dữ liệu với biến, tùy thuộc vào giá trị của nó. Vì các kiểu dữ liệu không được đặt theo nghĩa chặt chẽ, nên ta có thể thực hiện những việc như thêm chuỗi vào số nguyên mà không gây ra lỗi.
- Trong PHP 7, các khai báo kiểu đã được thêm vào. Việc này giúp ta chỉ định loại dữ liệu dự kiến ​​khi khai báo một hàm và bằng cách thêm khai báo nghiêm ngặt, nó sẽ đưa ra "Lỗi nghiêm trọng" nếu loại dữ liệu không khớp.

- Để chỉ định `strict`, chúng ta cần đặt khai báo `(strict_types=1);`. nó phải nằm trên dòng đầu tiên của tệp PHP.
**Ví dụ**
```php
<?php declare(strict_types=1); // strict requirement

function addNumbers(int $a, int $b) {
  return $a + $b;
}
echo addNumbers(5, "5 days");
// vì strict được bật và "5 ngày" không phải là số nguyên nên sẽ xảy ra lỗi
?>
```
> `strict` buộc mọi thứ phải được sử dụng theo cách đã định.

# Giá trị đối số mặc định PHP
**Ví dụ**
```php
<?php declare(strict_types=1); // strict requirement
function setHeight(int $minheight = 50) {
  echo "The height is : $minheight <br>";
}
setHeight(350);
setHeight(); // will use the default value of 50
setHeight(135);
?>

```
# PHP Functions - Returning values
- Để cho phép một function trả về một giá trị, hãy sử dụng câu lệnh `return`:

# PHP Return Type Declarations (Khai báo kiểu trả về PHP)
- PHP 7 cũng hỗ trợ Khai báo kiểu cho giá trị `return`.
- Để khai báo kiểu cho hàm trả về, hãy thêm dấu hai chấm ( `:` ) và kiểu ngay trước dấu ngoặc nhọn ( `{` ) khi khai báo hàm.
**Ví dụ**
```php
<?php declare(strict_types=1); // strict requirement
function addNumbers(float $a, float $b) : float {
  return $a + $b;
}
echo addNumbers(1.2, 5.2);
?>

```
- Ta có thể chỉ định một kiểu trả về khác với các kiểu đối số, nhưng hãy đảm bảo rằng kiểu trả về là đúng:
**Ví dụ**
```php
<?php declare(strict_types=1); // strict requirement
function addNumbers(float $a, float $b) : int {
  return (int)($a + $b);
}
echo addNumbers(1.2, 5.2);
?>

```

# Passing Arguments by Reference (Truyền đối số theo tham chiếu)
- Trong PHP, các đối số thường được truyền theo giá trị, có nghĩa là một bản sao của giá trị được sử dụng trong hàm và biến được truyền vào hàm không thể thay đổi.
- Khi một đối số hàm được truyền theo tham chiếu, các thay đổi đối với đối số cũng làm thay đổi biến được truyền vào. Để biến một đối số hàm thành một tham chiếu, toán tử `&` được sử dụng:
**Ví dụ**
```php
function add_five(&$value) {
  $value += 5;
}

$num = 2;
add_five($num);
echo $num;

```