# Các hàm sử dụng với mảng

- Các `array function` cho phép ta truy cập và thao tác với các mảng.

## Các hàm hay sử dụng nhất

1. **array_change_key_case($array, $case)**

- Chuyển tất cả các `key` trong mảng `$array` sang chữ hoa nếu `$case = 1` và sang chữ thường nếu `$case = 0`. Ta có thể dùng hằng số `CASE_UPPER` thay cho số `1` và `CASE_LOWER` thay cho số `0`.

2. **array_combine($array_keys, $array_values)**

- Trộn 2 mảng `$array_keys` và `$array_values` thành một mảng kết hợp với `$array_keys` là danh sách keys, `$array_value` là danh sách value tương ứng với key. Điều kiện là 2 mảng này phải bằng nhau.

3. **array_count_values( $array )**

- Đếm số lần xuất hiện của các phần tử giống nhau trong mảng `$array` và trả về một mảng kết quả.

4. **array_push(&$array, $add_value1, $add_value2, $add_value…)**

- Thêm vào cuối mảng `$array` một hoặc nhiều phần tử với các giá trị tương ứng biến `$add_value` truyền vào.

5. **array_pop(&$array)**

- Xóa trong mảng `$array` phần tử cuối cùng và trả về phần tử đã xóa.

6. **array_pad($array, $size, $value)**

- Kéo dãn mảng `$array` với kích thước là `$size`, và nếu kích thước truyền vào lớn hơn kích thước mảng `$array` thì giá trị $value được thêm vào, ngược lại nếu kích thước truyền vào nhỏ hơn kích thước mảng `$array`thì sẽ giữ nguyên. Nếu muốn giãn ở cuối mảng thì`$size` có giá trị dương, nếu muốn giãn ở đầu mảng thì `$size` có giá trị âm.

7. **array_shift(&$array)**

- Xóa phần tử đầu tiên ra khỏi mảng `$array` và trả về phần tử vừa xóa đó.

8. **array_unshift(&$array, $value1, $value2, …)**

- Thêm các giá trị `$value1`, `$value2`, … vào đầu mảng `$array`.

9. **is_array($variable)**

- Kiểm tra một biến có phải kiểu mảng hay không, kết quả trả về `true` nếu phải và `false` nếu không phải.

10. **in_array($needle, $haystackarray)**

- Kiểm tra giá trị $needle có nằm trong mảng `$haystackarray` không. trả về `true` nếu có và `flase` nếu không có.

11. **array_key_exists($key, $searcharray)**

- Kiểm tra key `$key` có tồn tại trong mảng `$searcharray` không, trả về `true` nếu có và `false` nếu không có.

12. **array_unique( $array )**

- Loại bỏ giá trị trùng trong mảng `$array`.

13. **array_values($array)**

- Chuyển mảng `$array` sang dạng mảng chỉ mục.

## Danh sách PHP Array Functions

| Chức năng                                                                                   | Mô tả                                                                                                                                                                |
| ------------------------------------------------------------------------------------------- | -------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| [array()](https://www.w3schools.com/php/func_array.asp)                                     | Tạo một mảng                                                                                                                                                         |
| [array_change_key_case()](https://www.w3schools.com/php/func_array_change_key_case.asp)     | Thay đổi tất cả các khóa trong một mảng thành chữ thường hoặc chữ hoa                                                                                                |
| [array_chunk()](https://www.w3schools.com/php/func_array_chunk.asp)                         | Chia một mảng thành nhiều mảng                                                                                                                                       |
| [array_column()](https://www.w3schools.com/php/func_array_column.asp)                       | Trả về các giá trị từ một cột duy nhất trong mảng đầu vào                                                                                                            |
| [array_combine()](https://www.w3schools.com/php/func_array_combine.asp)                     | Tạo một mảng bằng cách sử dụng các phần tử từ một mảng "keys" và một mảng "values"                                                                                   |
| [array_count_values()](https://www.w3schools.com/php/func_array_count_values.asp)           | Đếm tất cả các giá trị của một mảng                                                                                                                                  |
| [array_diff()](https://www.w3schools.com/php/func_array_diff.asp)                           | So sánh các mảng và trả về sự khác biệt (chỉ so sánh các giá trị)                                                                                                    |
| [array_diff_assoc()](https://www.w3schools.com/php/func_array_diff_assoc.asp)               | So sánh các mảng và trả về sự khác biệt (so sánh khóa và giá trị)                                                                                                    |
| [array_diff_key()](https://www.w3schools.com/php/func_array_diff_key.asp)                   | So sánh các mảng và trả về sự khác biệt (chỉ so sánh các khóa)                                                                                                       |
| [array_diff_uassoc()](https://www.w3schools.com/php/func_array_diff_uassoc.asp)             | So sánh các mảng và trả về sự khác biệt (so sánh khóa và giá trị, sử dụng hàm so sánh khóa do người dùng xác định)                                                   |
| [array_diff_ukey()](https://www.w3schools.com/php/func_array_diff_ukey.asp)                 | So sánh các mảng và trả về sự khác biệt (chỉ so sánh các khóa, sử dụng hàm so sánh khóa do người dùng xác định)                                                      |
| [array_fill()](https://www.w3schools.com/php/func_array_fill.asp)                           | Điền vào một mảng các giá trị                                                                                                                                        |
| [array_fill_keys()](https://www.w3schools.com/php/func_array_fill_keys.asp)                 | Điền vào một mảng các giá trị, chỉ định khóa                                                                                                                         |
| [array_filter()](https://www.w3schools.com/php/func_array_filter.asp)                       | Lọc các giá trị của một mảng bằng hàm gọi lại                                                                                                                        |
| [array_flip()](https://www.w3schools.com/php/func_array_flip.asp)                           | Lật/Trao đổi tất cả các khóa với các giá trị được liên kết của chúng trong một mảng                                                                                  |
| [array_intersect()](https://www.w3schools.com/php/func_array_intersect.asp)                 | So sánh các mảng và trả về kết quả khớp (chỉ so sánh các giá trị)                                                                                                    |
| [array_intersect_assoc()](https://www.w3schools.com/php/func_array_intersect_assoc.asp)     | So sánh các mảng và trả về kết quả khớp (so sánh khóa và giá trị)                                                                                                    |
| [array_intersect_key()](https://www.w3schools.com/php/func_array_intersect_key.asp)         | So sánh mảng, và trả về kết quả khớp (chỉ so sánh khóa)                                                                                                              |
| [array_intersect_uassoc()](https://www.w3schools.com/php/func_array_intersect_uassoc.asp)   | So sánh các mảng và trả về kết quả khớp (so sánh khóa và giá trị, sử dụng hàm so sánh khóa do người dùng xác định)                                                   |
| [array_intersect_ukey()](https://www.w3schools.com/php/func_array_intersect_ukey.asp)       | So sánh các mảng và trả về kết quả khớp (chỉ so sánh các khóa, sử dụng hàm so sánh khóa do người dùng xác định)                                                      |
| [array_key_exists()](https://www.w3schools.com/php/func_array_key_exists.asp)               | Kiểm tra xem khóa đã chỉ định có tồn tại trong mảng                                                                                                                  |
| [array_keys()](https://www.w3schools.com/php/func_array_keys.asp)                           | Trả về tất cả các khóa của một mảng                                                                                                                                  |
| [array_map()](https://www.w3schools.com/php/func_array_map.asp)                             | Gửi từng giá trị của một mảng tới một hàm do người dùng tạo, hàm này sẽ trả về các giá trị mới                                                                       |
| [array_merge()](https://www.w3schools.com/php/func_array_merge.asp)                         | Hợp nhất một hoặc nhiều mảng thành một mảng                                                                                                                          |
| [array_merge_recursive()](https://www.w3schools.com/php/func_array_merge_recursive.asp)     | Hợp nhất một hoặc nhiều mảng thành một mảng theo cách đệ quy                                                                                                         |
| [array_multisort()](https://www.w3schools.com/php/func_array_multisort.asp)                 | Sắp xếp các mảng nhiều hoặc nhiều chiều                                                                                                                              |
| [array_pad()](https://www.w3schools.com/php/func_array_pad.asp)                             | Chèn một số mục đã chỉ định, với một giá trị đã chỉ định, vào một mảng                                                                                               |
| [array_pop()](https://www.w3schools.com/php/func_array_pop.asp)                             | Xóa phần tử cuối cùng của một mảng                                                                                                                                   |
| [array_product()](https://www.w3schools.com/php/func_array_product.asp)                     | Tính tích của các giá trị trong một mảng                                                                                                                             |
| [array_push()](https://www.w3schools.com/php/func_array_push.asp)                           | Chèn một hoặc nhiều phần tử vào cuối mảng                                                                                                                            |
| [array_rand()](https://www.w3schools.com/php/func_array_rand.asp)                           | Trả về một hoặc nhiều khóa ngẫu nhiên từ một mảng                                                                                                                    |
| [array_reduce()](https://www.w3schools.com/php/func_array_reduce.asp)                       | Trả về một mảng dưới dạng một chuỗi, sử dụng hàm do người dùng xác định                                                                                              |
| [array_replace()](https://www.w3schools.com/php/func_array_replace.asp)                     | Thay thế các giá trị của mảng đầu tiên bằng các giá trị từ các mảng sau                                                                                              |
| [array_replace_recursive()](https://www.w3schools.com/php/func_array_replace_recursive.asp) | Thay thế các giá trị của mảng đầu tiên bằng các giá trị từ các mảng sau theo cách đệ quy                                                                             |
| [array_reverse()](https://www.w3schools.com/php/func_array_reverse.asp)                     | Trả về một mảng theo thứ tự ngược lại                                                                                                                                |
| [array_search()](https://www.w3schools.com/php/func_array_search.asp)                       | Tìm kiếm một mảng cho một giá trị đã cho và trả về khóa                                                                                                              |
| [array_shift()](https://www.w3schools.com/php/func_array_shift.asp)                         | Xóa phần tử đầu tiên khỏi một mảng và trả về giá trị của phần tử đã xóa                                                                                              |
| [array_slice()](https://www.w3schools.com/php/func_array_slice.asp)                         | Trả về các phần đã chọn của một mảng                                                                                                                                 |
| [array_splice()](https://www.w3schools.com/php/func_array_splice.asp)                       | Xóa và thay thế các phần tử đã chỉ định của một mảng                                                                                                                 |
| [array_sum()](https://www.w3schools.com/php/func_array_sum.asp)                             | Trả về tổng giá trị trong một mảng                                                                                                                                   |
| [array_udiff()](https://www.w3schools.com/php/func_array_udiff.asp)                         | So sánh các mảng và trả về sự khác biệt (chỉ so sánh các giá trị, sử dụng hàm so sánh khóa do người dùng xác định)                                                   |
| [array_udiff_assoc()](https://www.w3schools.com/php/func_array_udiff_assoc.asp)             | So sánh các mảng và trả về sự khác biệt (so sánh các khóa và giá trị, sử dụng hàm tích hợp để so sánh các khóa và hàm do người dùng xác định để so sánh các giá trị) |
| [array_udiff_uassoc()](https://www.w3schools.com/php/func_array_udiff_uassoc.asp)           | So sánh các mảng và trả về sự khác biệt (so sánh khóa và giá trị, sử dụng hai hàm so sánh khóa do người dùng xác định)                                               |
| [array_uintersect()](https://www.w3schools.com/php/func_array_uintersect.asp)               | So sánh các mảng và trả về kết quả khớp (chỉ so sánh giá trị, sử dụng hàm so sánh khóa do người dùng xác định)                                                       |
| [array_uintersect_assoc()](https://www.w3schools.com/php/func_array_uintersect_assoc.asp)   | So sánh các mảng và trả về kết quả khớp (so sánh khóa và giá trị, sử dụng hàm tích hợp để so sánh khóa và hàm do người dùng xác định để so sánh giá trị)             |
| [array_uintersect_uassoc()](https://www.w3schools.com/php/func_array_uintersect_uassoc.asp) | So sánh mảng, và trả về kết quả khớp (so sánh khóa và giá trị, sử dụng hai hàm so sánh khóa do người dùng xác định)                                                  |
| [array_unique()](https://www.w3schools.com/php/func_array_unique.asp)                       | Xóa các giá trị trùng lặp khỏi một mảng                                                                                                                              |
| [array_unshift()](https://www.w3schools.com/php/func_array_unshift.asp)                     | Thêm một hoặc nhiều phần tử vào đầu mảng                                                                                                                             |
| [array_values()](https://www.w3schools.com/php/func_array_values.asp)                       | Trả về tất cả các giá trị của một mảng                                                                                                                               |
| [array_walk()](https://www.w3schools.com/php/func_array_walk.asp)                           | Áp dụng hàm người dùng cho mọi thành viên của một mảng                                                                                                               |
| [array_walk_recursive()](https://www.w3schools.com/php/func_array_walk_recursive.asp)       | Áp dụng đệ quy hàm người dùng cho mọi thành viên của một mảng                                                                                                        |
| [arsort()](https://www.w3schools.com/php/func_array_arsort.asp)                             | Sắp xếp một mảng kết hợp theo thứ tự giảm dần, theo giá trị                                                                                                          |
| [asort()](https://www.w3schools.com/php/func_array_asort.asp)                               | Sắp xếp một mảng kết hợp theo thứ tự tăng dần, theo giá trị                                                                                                          |
| [compact()](https://www.w3schools.com/php/func_array_compact.asp)                           | Tạo mảng chứa các biến và giá trị của chúng                                                                                                                          |
| [count()](https://www.w3schools.com/php/func_array_count.asp)                               | Trả về số phần tử trong một mảng                                                                                                                                     |
| [current()](https://www.w3schools.com/php/func_array_current.asp)                           | Trả về phần tử hiện tại trong một mảng                                                                                                                               |
| [each()](https://www.w3schools.com/php/func_array_each.asp)                                 | Không dùng nữa từ PHP 7.2. Trả về cặp khóa và giá trị hiện tại từ một mảng                                                                                           |
| [end()](https://www.w3schools.com/php/func_array_end.asp)                                   | Đặt con trỏ bên trong của một mảng thành phần tử cuối cùng của nó                                                                                                    |
| [extract()](https://www.w3schools.com/php/func_array_extract.asp)                           | Nhập các biến vào bảng ký hiệu hiện tại từ một mảng                                                                                                                  |
| [in_array()](https://www.w3schools.com/php/func_array_in_array.asp)                         | Kiểm tra xem một giá trị đã chỉ định có tồn tại trong một mảng hay không                                                                                             |
| [key()](https://www.w3schools.com/php/func_array_key.asp)                                   | Tìm nạp khóa từ một mảng                                                                                                                                             |
| [krsort()](https://www.w3schools.com/php/func_array_krsort.asp)                             | Sắp xếp một mảng kết hợp theo thứ tự giảm dần, theo từ khóa                                                                                                          |
| [ksort()](https://www.w3schools.com/php/func_array_ksort.asp)                               | Sắp xếp một mảng kết hợp theo thứ tự tăng dần, theo khóa                                                                                                             |
| [list()](https://www.w3schools.com/php/func_array_list.asp)                                 | Gán các biến như thể chúng là một mảng                                                                                                                               |
| [natcasesort()](https://www.w3schools.com/php/func_array_natcasesort.asp)                   | Sắp xếp một mảng bằng thuật toán "thứ tự tự nhiên" không phân biệt chữ hoa chữ thường                                                                                |
| [natsort()](https://www.w3schools.com/php/func_array_natsort.asp)                           | Sắp xếp một mảng bằng thuật toán "thứ tự tự nhiên"                                                                                                                   |
| [next()](https://www.w3schools.com/php/func_array_next.asp)                                 | Nâng cao con trỏ mảng bên trong của một mảng                                                                                                                         |
| [pos()](https://www.w3schools.com/php/func_array_pos.asp)                                   | Bí danh của [current()](https://www.w3schools.com/php/func_array_current.asp)                                                                                        |
| [prev()](https://www.w3schools.com/php/func_array_prev.asp)                                 | Tua lại con trỏ mảng bên trong                                                                                                                                       |
| [range()](https://www.w3schools.com/php/func_array_range.asp)                               | Tạo một mảng chứa nhiều phần tử                                                                                                                                      |
| [reset()](https://www.w3schools.com/php/func_array_reset.asp)                               | Đặt con trỏ bên trong của một mảng thành phần tử đầu tiên của nó                                                                                                     |
| [rsort()](https://www.w3schools.com/php/func_array_rsort.asp)                               | Sắp xếp một mảng được lập chỉ mục theo thứ tự giảm dần                                                                                                               |
| [shuffle()](https://www.w3schools.com/php/func_array_shuffle.asp)                           | Xáo trộn một mảng                                                                                                                                                    |
| [sizeof()](https://www.w3schools.com/php/func_array_sizeof.asp)                             | Bí danh của [count()](https://www.w3schools.com/php/func_array_count.asp)                                                                                            |
| [sort()](https://www.w3schools.com/php/func_array_sort.asp)                                 | Sắp xếp một mảng được lập chỉ mục theo thứ tự tăng dần                                                                                                               |
| [uasort()](https://www.w3schools.com/php/func_array_uasort.asp)                             | Sắp xếp một mảng theo các giá trị bằng hàm so sánh do người dùng xác định                                                                                            |
| [uksort()](https://www.w3schools.com/php/func_array_uksort.asp)                             | Sắp xếp một mảng theo các khóa bằng hàm so sánh do người dùng xác định                                                                                               |
| [usort()](https://www.w3schools.com/php/func_array_usort.asp)                               | Sắp xếp một mảng bằng hàm so sánh do người dùng định nghĩa                                                                                                           |
