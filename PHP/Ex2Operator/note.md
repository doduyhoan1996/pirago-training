# Các toán tử trong php
## PHP Operators
- Các toán tử được sử dụng để thực hiện các thao tác trên các biến và giá trị.
### PHP chia các toán tử thành các nhóm sau:
- Toán tử số học (Arithmetic operators)
- Toán tử gán (Assignment operators)
- So sánh các toán tử (Comparison operators)
- Toán tử tăng/giảm (Increment/Decrement operators)
- Toán tử logic (Logical operators)
- Toán tử chuỗi (String operators)
- Toán tử mảng (Array operators)
- Toán tử gán có điều kiện (Conditional assignment operators)

# Toán tử số học PHP
- Các toán tử số học PHP được sử dụng với các giá trị số để thực hiện các phép toán số học phổ biến, chẳng hạn như cộng, trừ, nhân, v.v.

| Operator    |	Name               | Example         | Result          
| ----------- |	------------------ | ----------------| -------------------------------------------     
| +           |	Phép cộng          | $x + $y         | Tổng của $x và $y
| -           |	phép trừ           | $x - $y         | Hiệu của $x và $y	
| *           |	Phép nhân          | $x * $y	     | Tích $x và $y	
| /           |	Phép chia          | $x / $y	     | Thương $x và $y	
| %           |	Phép chia lấy dư.  | $x % $y         | Số dư của $x chia cho $y
| **          |	lũy thừa           | $x ** $y        | Kết quả $x lũy thừa $y 

# Toán tử gán PHP
- Các toán tử gán PHP được sử dụng với các giá trị số để ghi một giá trị cho một biến.

| Assignment  |	Same as...         | Description 
| ----------- |	------------------ | ----------------
| x = y	      |	x = y	           | Toán hạng bên trái được đặt thành giá trị của biểu thức bên phải       
| x += y      |	x = x + y          | Phép cộng          
| x -= y      |	x = x - y          | phép trừ  	     
| x *= y      |	x = x * y          | Phép nhân 	     
| x /= y      |	 x = x / y         | Phép chia        
| x %= y      |	x = x % y          | Phép chia lấy dư       

# Toán tử so sánh PHP
- Toán tử so sánh PHP được sử dụng để so sánh hai giá trị (số hoặc chuỗi):

| Operator    |	Name                 | Example         | Result          
| ----------- |	-------------------- | ----------------| -------------------------------------------     
| ==          |	Bằng                 | $x == $y        | Trả về true nếu $x bằng $y
| ===         |	Giống cả type        | $x === $y       | Trả về true nếu $x bằng $y và chúng cùng loại	
| !=          |	So sánh khác         | $x != $y	       | Trả về true nếu $x không bằng $y	
| <>          |	So sánh khác         | $x <> $y	       | Trả về true nếu $x không bằng $y	
| !==         |	khác hoặc khác type  | $x !== $y       | Trả về true nếu $x không bằng $y hoặc chúng không cùng loại
| >           |	Lớn hơn              | $x > $y         | Trả về true nếu $x lớn hơn $y 
| <           | Nhỏ hơn              | $x < $y         | Trả về true nếu $x nhỏ hơn $y
| >=          | Lớn hơn hoặc bằng    | $x >= $y        | Trả về true nếu $x lớn hơn hoặc bằng $y
| <=          | Nhỏ hơn hoặc bằng    | $x <= $y        | Trả về true nếu $x nhỏ hơn hoặc bằng $y         
| <=>         | Spaceship            | $x <=> $y       | Trả về 1 số nguyên nhỏ hơn, bằng hoặc lớn hơn 0, tùy thuộc vào việc $x nhỏ hơn, bằng hoặc lớn hơn $y. Được giới thiệu trong PHP 7.

# Toán tử tăng / giảm trong PHP

| Operator	  | Name	              | Description
| ----------- |	----------------------| --------------------------------------
| ++$x	      | Tăng trước            | Tăng $x lên một, sau đó trả về $x	
| $x++	      | Tăng sau	          | Trả về $x, sau đó tăng $x lên một	
| --$x	      | Giảm trước	          | Giảm $x đi một, sau đó trả về $x	
| $x--	      | Giảm sau              | Trả về $x, sau đó giảm $x đi một

# Toán tử logic PHP
- Các toán tử logic PHP được sử dụng để kết hợp các câu điều kiện.

| Operator	   | Name	       | Example	       | Result
| ------------ | ------------- | ------------------| ----------------------------------------------------  	
| and	       | And	       | $x and $y	       | True nếu cả $x và $y đều đúng	
| or	       | Or	           | $x or $y	       | True nếu $x hoặc $y là đúng	
| xor	       | Xor	       | $x xor $y	       | True nếu $x hoặc $y đều đúng, nhưng không phải cả hai	
| &&	       | And	       | $x && $y	       | True nếu cả $x và $y đều đúng	
| \|\|	       | Or	           | $x \|\| $y	       | True nếu $x hoặc $y là đúng	
| !	           | Not	       | !$x	           | True nếu $x không đúng

# Toán tử chuỗi PHP
- PHP có hai toán tử được thiết kế đặc biệt cho chuỗi.

| Operator	    | Name	                    | Example	        | Result
| ------------- | ------------------------- | ------------------| -------------------
| .	            | Concatenation	            | $txt1 . $txt2	    | Nối $txt1 và $txt2	
| .=	        | Concatenation assignment	| $txt1 .= $txt2    | Nối $txt2 vào $txt1

# Toán tử mảng PHP
- Các toán tử mảng PHP được sử dụng để so sánh các mảng.

| Operator	    | Name	            | Example           | Result
| ------------- | ----------------- | ------------------| -------------------
| +	            | Union	            | $x + $y	        | Hợp của $x và $y
| ==	        | Equality	        | $x == $y	        | Trả về true nếu $x và $y có cùng cặp khóa/giá trị	
| ===	        | Identity	        | $x === $y	        | Trả về true nếu $x và $y có cùng cặp khóa/giá trị theo cùng thứ tự và cùng loại	
| !=	        | Inequality	    | $x != $y	        | Trả về true nếu $x không bằng $y
| <>	        | Inequality	    | $x <> $y	        | Trả về true nếu $x không bằng $y
| !==	        | Non-identity	    | $x !== $y	        | Trả về true nếu $x không giống với $y

# Toán tử gán có điều kiện PHP
- Toán tử gán điều kiện PHP được sử dụng để đặt giá trị tùy thuộc vào điều kiện:

| Operator	| Name	            | Example	                    | Result
| --------- | ----------------- | ------------------------------| --------------------------------------------------------------------------------------------------------------------
| ?:	    | Ternary	        | $x = expr1 ? expr2 : expr3	| Trả về giá trị của $x. Giá trị của $x là expr2 nếu expr1 = TRUE Giá trị của $x là expr3 nếu expr1 = FALSE
| ??	    | Null coalescing	| $x = expr1 ?? expr2	        | Trả về giá trị của $x. Giá trị của $x là expr1 nếu expr1 tồn tại và không phải là NULL. Nếu expr1 không tồn tại, hoặc là NULL, giá trị của $x là expr2. Được giới thiệu trong PHP 7

