# Lập trình hướng đổi tượng
# Lập trình hướng đối tượng (OOP) là gì?
>**Lập trình hướng đối tượng (Object Oriented Programming, viết tắt: OOP)** là một kỹ thuật lập trình cho phép  lập trình viên tạo ra các đối tượng trong code trừu tượng hóa các đối tượng thực tế trong cuộc sống.
## Đối tượng (Object)
- **Đối tượng (Object)** có thể là con người, điện thoại, máy tính, ... và điểm chung là đều gồm 2 thành phần chính là:
    - **Thuộc tính (Attribute)**: là những thông tin, đặc điểm của đối tượng. Ví dụ: con người có các đặc tính như mắt, mũi, tay, chân, ...
    - **Phương thức (Method)**: là những thao tác, hành động mà đối tượng đó có thể thực hiện. Ví dụ: một người sẽ có thể thực hiện hành động nói, đi, ăn, uống, ...
## Lớp (Class)
- Một lớp là một kiểu dữ liệu bao gồm các thuộc tính và các phương thức được định nghĩa từ trước. Đây là sự trừu tượng hóa của đối tượng. Khác với kiểu dữ liệu thông thường, một lớp là một đơn vị (trừu tượng) bao gồm sự kết hợp giữa các phương thức và các thuộc tính. Hiểu nôm na hơn là các đối tượng có các đặc tính tương tự nhau được gom lại thành một lớp đối tượng.

## Sự khác nhau giữa đối tượng và lớp
- Lớp có thể hiểu nó như là khuôn mẫu, đối tượng là một thực thể thể hiện dựa trên khuôn mẫu đó. Ví dụ: Ta nói về loài chó, có thể hiểu nó là class (lớp) chó có:
    - Các thông tin, đặc điểm: 4 chân, 2 mắt, có đuôi, có chiều cao, có cân nặng, màu lông…
    - Các hành động như: sủa, đi, ăn, ngủ…
- Đối tượng thì chính là con chó Phú Quốc ta đang nuôi trong nhà cũng mang đặc tính của lớp chó.

# Các đặc điểm cơ bản của lập trình hướng đối tượng:
## Tính đóng gói (Encapsulation)
- Mọi dữ liệu và phương thức có liên quan đều sẽ được đóng gói thành các lớp để tiện lợi hơn cho quá trình sử dụng và quản lý. Nhờ vậy mà mỗi lớp đều sẽ được xây dựng để thực hiện một nhóm chức năng đặc trưng riêng của lớp đó. Việc đóng gói sẽ giúp che dấu một số thông tin và chi tiết cài đặt nội bộ khiến bên ngoài không thể nhìn thấy.

## Inheritance (tính năng kế thừa)
- Tính kế thừa sẽ cho phép bạn xây dựng một lớp mới dựa trên những định nghĩa đã có của lớp đó. Điều này có nghĩa là: Lớp cha có khả năng chia sẻ dữ liệu cũng như phương thức cho các lớp con. Từ đó, các lớp con không cần phải định nghĩa lại mà còn có thể mở rộng thành phần kế thừa để bổ sung thêm các thành phần mới.
- Việc tái sử dụng một mã nguồn tối ưu sẽ tận dụng được mã nguồn và mọi loại kế thừa thường sẽ gặp: đơn kế thừa, kế thừa đa cấp, đa kế thừa và kế thừa thứ bậc. Khi xây dựng, thường sẽ bắt đầu thiết kế cho định nghĩa của các lớp trước và các lớp này sẽ có quan hệ với một số lớp khác nên chúng có đặc tính giống nhau.

# Polymorphism (Tính đa hình)
- Đây là hành động có thể được thực hiện bởi nhiều cách khác nhau và nó là tính chất thể hiện việc chứa đựng sức mạnh của một lập trình hướng đối tượng. Hiểu như sau: Đa hình là khái niệm mà hai hoặc nhiều lớp sẽ có những phương thức tương đối giống nhau nhưng nó lại có  thể thực thi theo nhiều cách khác.

# Abstraction (Tính trừu tượng)
- Tính trừu tượng thể hiện tổng quát hóa lên một cái gì đó mà không cần chú ý các chi tiết bên trong. Nó sẽ không màng đến những chi tiết bên trong là gì mà người ta vẫn có thể hiểu nó ngay mỗi khi nó được nhắc đến.
- Trong lập trình OOP thì tính trừu tượng có nghĩa là chọn ra các phương thức, các thuộc tính của đối tượng cần cho việc giải quyết các bài toán lập trình. Bởi vì đối tượng sẽ có rất nhiều thuộc tính phương thức nhưng với bài toán cụ thể thì bạn không nhất thiết phải chọn toàn bộ.

# Ưu điểm của lập trình hướng đối tượng OOP là gì?
### Trở thành một trong những lập trình có tầm quan trọng lớn và được sử dụng phổ biến bởi vì OOP sở hữu nhiều đặc điểm nổi bật như sau:
- Code OOP có thể sử dụng lại nên giúp cho các lập trình viên tiết kiệm được tài nguyên.
- OOP mô hình hóa được những thứ phức tạo dưới dạng các cấu trúc đơn giản.
- OOP giúp quá trình sửa lỗi trở nên dễ dàng hơn, so với việc tìm lỗi ở nhiều vị trí khác trong code thì tìm lỗi ở trong các lớp sẽ đơn giản và ít mất thời gian hơn.
- OOP có tính bảo mật cao, có khả năng bảo vệ mọi thông tin thông qua việc đóng gói.
- Sử dụng OOP rất mở rộng được dự án.