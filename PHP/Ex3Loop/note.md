# Các loại vòng lặp trong php
# PHP Loops
- Thông thường, khi bạn viết mã, bạn muốn cùng một khối mã chạy đi chạy lại một số lần nhất định. Vì vậy, thay vì thêm một số dòng mã gần như bằng nhau trong một tập lệnh, chúng ta có thể sử dụng các vòng lặp.
- Các vòng lặp được sử dụng để thực thi lặp đi lặp lại cùng một khối mã, miễn là một điều kiện nhất định là đúng.
*Trong PHP, chúng ta có các loại vòng lặp sau:*
- `while` - lặp qua một khối mã miễn là điều kiện được chỉ định là đúng
- `do...while` - lặp qua một khối mã một lần, sau đó lặp lại vòng lặp miễn là điều kiện đã chỉ định là đúng
- `for` - lặp qua một khối mã một số lần được chỉ định
- `foreach` - lặp qua một khối mã cho từng phần tử trong một mảng

## PHP while Loop
**cú pháp**
```php
while (condition is true) {
    //   code to be executed;
}
```

## PHP do while Loop
**cú pháp**
```php
do {
    //   code to be executed;
} while (condition is true);
```

## PHP for Loop
**cú pháp**
```php
for (init counter; test counter; increment counter) {
    //   code to be executed for each iteration;
}
```

## PHP foreach Loop
- Vòng lặp `foreach` chỉ hoạt động trên mảng và được sử dụng để lặp qua từng cặp `key/value` trị trong một mảng.

**cú pháp**
```php
foreach ($array as $value) {
    //   code to be executed;
}
```
