# Exception and try - catch

# PHP Exception Handling

- Exceptions được sử dụng để thay đổi luồng thông thường của tập lệnh nếu xảy ra lỗi được chỉ định.

## What is an Exception (Ngoại lệ là gì)

- Đây là điều thường xảy ra khi một ngoại lệ được kích hoạt:
  - Trạng thái code hiện tại được lưu
  - Việc thực thi code sẽ chuyển sang chức năng xử lý ngoại lệ (tùy chỉnh) được xác định trước
  - Tùy thuộc vào tình huống, trình xử lý sau đó có thể tiếp tục thực thi từ trạng thái mã đã lưu, chấm dứt thực thi tập lệnh hoặc tiếp tục tập lệnh từ một vị trí khác trong code
- Các phương pháp xử lý lỗi khác nhau:
  - Sử dụng cơ bản các ngoại lệ **(Basic use of Exceptions)**
  - Tạo trình xử lý ngoại lệ tùy chỉnh **(Creating a custom exception handler)**
  - Nhiều ngoại lệ **(Multiple exceptions)**
  - Ném lại một ngoại lệ **(Re-throwing an exception)**
  - Đặt trình xử lý ngoại lệ cấp cao nhất **(Setting a top level exception handler)**
    > Lưu ý: Các Exceptions chỉ nên được sử dụng với các điều kiện lỗi và không được sử dụng để nhảy đến một vị trí khác trong mã tại một điểm đã chỉ định.

## Basic use of Exceptions

- Khi một `exception` được `thrown`, code theo sau nó sẽ không được thực thi và PHP sẽ cố gắng tìm khối `"catch"` phù hợp.
- Nếu một ngoại lệ không bị bắt, một lỗi nghiêm trọng sẽ được đưa ra với thông báo `"Uncaught Exception"`.

## Try, throw and catch

- Exception code thích hợp nên bao gồm:
  1. `try` - Một hàm sử dụng ngoại lệ phải nằm trong khối "try". Nếu exception không kích hoạt, code sẽ tiếp tục như bình thường. Tuy nhiên, nếu exception kích hoạt, một exception sẽ được "thrown"
  2. `throw` - Đây là cách ta kích hoạt một exception. Mỗi lần "throw" phải có ít nhất một lần "catch"
  3. `catch` - Một khối "`catch`" truy xuất một exception và tạo một đối tượng chứa thông tin exception

## Creating a Custom Exception Class

- Để tạo một `custom exception handler`, ta phải tạo một class đặc biệt với các hàm có thể được gọi khi có exception xảy ra trong PHP. Class phải là một extension của exception class.
- Lớp ngoại lệ tùy chỉnh kế thừa các thuộc tính từ exception class của PHP và ta có thể thêm các chức năng tùy chỉnh vào nó.

## Multiple Exceptions

- Tập lệnh có thể sử dụng nhiều ngoại lệ để kiểm tra nhiều điều kiện.
- Có thể sử dụng một số khối `if..else`, một công tắc hoặc lồng nhiều ngoại lệ. Các ngoại lệ này có thể sử dụng các lớp ngoại lệ khác nhau và trả về các thông báo lỗi khác nhau

## Re-throwing Exceptions

- Đôi khi, khi một exception được thrown, bạn có thể muốn xử lý nó khác với cách tiêu chuẩn. Có thể throw exception lần thứ hai trong khối "catch".
- Tập lệnh nên ẩn lỗi hệ thống khỏi người dùng. Lỗi hệ thống có thể quan trọng đối với coder, nhưng không được người dùng quan tâm. Để giúp người dùng dễ dàng hơn, bạn có thể ném lại ngoại lệ bằng thông báo thân thiện với người dùng

## Set a Top Level Exception Handler

- Hàm `set_Exception_handler()` thiết lập một hàm do người dùng xác định để xử lý tất cả các ngoại lệ chưa được phát hiện:

```php
<?php
function myException($exception) {
  echo "<b>Exception:</b> " . $exception->getMessage();
}

set_exception_handler('myException');

throw new Exception('Uncaught Exception occurred');
?>

```

**Hiển thị:**

```
Exception: Uncaught Exception occurred
```

# Rules for exceptions (Quy tắc cho các trường hợp ngoại lệ)

- Code có thể được bao quanh trong một `try block`, để giúp catch được `exceptions` tiềm ẩn
- Mỗi `try block` hoặc `throw` phải có ít nhất một `catch block` tương ứng
- Nhiều `catch block` có thể được sử dụng để `catch` các loại `exception` khác nhau
- `Exceptions` có thể được `thrown` (hoặc `re-thrown`) trong một `catch block` trong một `try block`
  > Một quy tắc đơn giản: Nếu bạn `throw` thứ gì đó, bạn phải `catch` lấy nó.
