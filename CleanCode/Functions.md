# Functions

- Nhỏ
	+ Các hàm nên rất nhỏ.
	+ Các hàm không nên `“chạm nóc”` 100 dòng, và độ dài thích hợp nhất dành cho hàm là không quá 20 dòng lệnh.
- Các khối lệnh và thụt dòng
- Thực hiện MỘT việc
	+ `“HÀM CHỈ NÊN THỰC HIỆN MỘT VIỆC. CHÚNG NÊN LÀM TỐT VIỆC ĐÓ, VÀ CHỈ LÀM DUY NHẤT VIỆC ĐÓ”`
- Mỗi hàm là một cấp độ trừu tượng
	+ Việc trộn lẫn các cấp độ trừu tượng với nhau trong một hàm sẽ luôn gây ra những hiểu lầm cho người đọc.
	+ Đọc code từ trên xuống dưới: Nguyên tắc Stepdown
		.Làm cho code của bạn đọc như một đoạn văn là kỹ thuật hiệu quả để duy trì sự đồng nhất của các cấp trừu tượng.
- Câu lệnh switch
	+ Nguyên tắc Đơn nhiệm: Mỗi lớp chỉ nên chịu trách nhiệm về một nhiệm vụ cụ thể nào đó mà thôi.
	+ Nguyên tắc Đóng & mở: Chúng ta nên hạn chế việc chỉnh sửa bên trong một Class hoặc Module có sẵn, thay vào đó hãy xem xét mở rộng chúng.
- Dùng tên có tính mô tả
	+ Nguyên tắc của Ward: “Bạn biết bạn đang làm việc cùng code sạch là khi việc đọc code hóa ra yomost hơn những gì bạn mong đợi”.
	+ Hàm càng nhỏ và càng cô đặc thì càng dễ chọn tên mô tả cho nó hơn.
	+ Một tên dài nhưng mô tả đầy đủ chức năng của hàm luôn tốt hơn những cái tên ngắn.
	+ Một tên dài thì tốt hơn một ghi chú (comment) dài.
	+ Dùng một nguyên tắc đặt tên cho phép dễ đọc nhiều từ trong tên hàm, những từ đó sẽ cho ta biết hàm đó hoạt động ra sao.
	+ Đừng ngại dành thời gian cho việc chọn tên. Sử dụng IDE và thử đặt các tên khác nhau cho đến khi tìm thấy một cái tên có tính mô tả.
	=> Chọn một cái tên có tính mô tả tốt sẽ giúp ta vẽ lại thiết kế của mô-đun đó vào não, và việc cải thiện nó sẽ đơn giản hơn. Nhưng điều đó không có nghĩa là ta sẽ bất chấp tất cả để “săn” được một cái tên tốt hơn để thay thế tên hiện tại.
- Đối số của hàm
	+ Số lượng tham số lý tưởng trong hàm là 0 `(niladic)`, 1 `(monadic)`, 2 `(dyadic)`. Với 3 tham số `(triadic)` nên xem xét kỹ lưỡng, tránh được nếu có thể. Còn các trường hợp nhiều hơn thì thực sự không nên dùng.
- Không có tác dụng phụ
	+ Đối số đầu ra: Ta nên tránh các đối số đầu ra. Nếu hàm của bạn phải thay đổi trạng thái của một cái gì đó, hãy thay đổi trạng thái của đối tượng sở hữu nó.
- Tách lệnh truy vấn
	+ Hàm nên làm một cái gì đó hoặc trả lời một cái gì đó, nhưng không phải cả hai
- Đừng lặp lại code của bạn
- Lập trình có cấu trúc
