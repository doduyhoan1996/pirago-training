# Smells and Heuristics
- Những khó chịu trong code

## Comments
### C1: Thông tin không phù hợp
- Comments nên được dành riêng cho ghi chú kỹ thuật về mã và thiết kế.
### C2: Comments lỗi thời
- Tốt nhất là không nên viết comments, hạn chế vì nó sẽ trở nên lỗi thời.
### C3: Comments thừa
- Comments nên nói những điều mà code không thể nói cho chính nó.
### C4: Comments viết kém
- Một Comment đáng viết là đáng để viết tốt.
### C5: Commented-Out Code
- Khi bạn thấy `Commented-Out Code`, hãy xóa nó !

## Environment
### E1: Xây dựng yêu cầu nhiều hơn một bước
### E2: Tests Require yêu cầu nhiều hơn một bước
- Có thể chạy tất cả các bài `test unit` chỉ bằng một lệnh.
- Việc có thể chạy tất cả các bài `test` là rất cơ bản và quan trọng đến mức nó phải được thực hiện nhanh chóng, dễ dàng và rõ ràng.

## Functions
### F1: Quá nhiều đối số (Too Many Arguments)
- `Functions` nên có ít đối số.
### F2: Đối số đầu ra (Output Arguments)
### F3: Đối số cờ (Flag Arguments)
- Đối số `Boolean` tuyên bố rằng hàm thực hiện nhiều hơn một việc.
### F4: Dead Function
- `Methods` không bao giờ được gọi nên bị loại bỏ.

## Tổng quan (General)
### G1: Nhiều ngôn ngữ trong một tệp nguồn
### G2: Hành vi rõ ràng không được thực hiện (Obvious behavior Is Unimplemented)
### G3: Hành vi không đúng tại ranh giới (Incorrect Behavior at the Boundaries)
### G4: Két an toàn bị ghi đè (Overridden Safeties)
### G5: Sao chép (Duplication)
### G6: Code ở mức độ trừu tượng sai (Code at Wrong Level of Abstraction)
### G7: Các lớp cơ sở tùy thuộc vào các dẫn xuất của chúng (Base Classes Depending on Their Derivatives)
### G8: Quá nhiều thông tin
### G9: Dead Code
- Xóa nó khỏi hệ thống.
### G10: Tách dọc (Vertical Separation)
- Các biến và function nên được xác định gần nơi chúng được sử dụng.
### G11: Không nhất quán (Inconsistency)
- Nếu ta làm điều gì đó theo một cách nhất định, hãy làm tất cả những điều tương tự theo cùng một cách.
### G12: Lộn xộn (Clutter)
### G13: Khớp nối nhân tạo (Artificial Coupling)
- Những thứ không phụ thuộc vào nhau không nên được kết hợp một cách giả tạo.
- khớp nối nhân tạo là khớp nối giữa hai mô-đun không phục vụ mục đích trực tiếp.
### G14: Feature Envy
### G15: Đối số bộ chọn (Selector Arguments)
- Tốt hơn là có nhiều functions hơn là chuyển một số code vào một functions để chọn hành vi.
### G16: Ý định bị che khuất (Obscured Intent)
- code càng biểu cảm càng tốt. Các biểu thức `run-on`, ký hiệu `Hungary` và `magic numbers` đều che khuất ý định của tác giả.
### G17: Đặt nhầm trách nhiệm (Misplaced Responsibility)
- Một trong những quyết định quan trọng nhất mà nhà phát triển phần mềm có thể đưa ra là đặt code ở đâu.
- Code nên được đặt ở nơi mà người đọc mong đợi một cách tự nhiên.
- Một cách để đưa ra quyết định này là xem tên của các `functions`.
### G18: Static không phù hợp (Inappropriate Static)
- Nên ưu tiên `nonstatic methods` hơn `static methods`. Khi nghi ngờ, hãy làm `function nonstatic`.
### G19: Sử dụng biến giải thích (Use Explanatory Variables)
### G20: Function Names nên nói những gì họ làm (Function Names Should Say What They Do)
- Nên cố gắng tìm một cái tên hay hơn hoặc sắp xếp lại chức năng để nó có thể được đặt trong các hàm có tên hay hơn.
### G21: Tìm hiểu thuật toán (Understand the Algorithm)
- Trước khi bạn cho rằng mình đã hoàn thành một chức năng, hãy đảm bảo rằng bạn hiểu cách thức hoạt động của nó.
### G22: Biến các phụ thuộc logic thành vật lý (Make Logical Dependencies Physical)
### G23: Ưu tiên Đa hình hơn If/Else hoặc Switch/Case (Prefer Polymorphism to If/Else or Switch/Case)
### G24: Tuân thủ các quy ước tiêu chuẩn (Follow Standard Conventions)
- Mọi người trong nhóm nên tuân theo các quy ước này.
### G25: thay thế `magic Numbers` bằng `Named Constants` (replace `magic Numbers` with `Named Constants`)
- Nói chung, bạn không nên có số liệu thô trong code của mình. bạn nên ẩn chúng đằng sau các hằng số được đặt tên tốt.
- Thuật ngữ `“magic Numbers”` không chỉ áp dụng cho các số. Nó áp dụng cho bất kỳ code thông báo nào có giá trị không tự mô tả.
### G26: Chính xác (Be Precise)
- Nếu bạn quyết định gọi một hàm có thể trả về `null` , hãy đảm bảo rằng bạn đã kiểm tra `null` .
- Sự mơ hồ và thiếu chính xác trong mã là kết quả của sự bất đồng hoặc lười biếng. Trong cả hai trường hợp, chúng nên được loại bỏ.
### G27: `Structure` thay vì `Convention` (`Structure` over `Convention`)
- Thực thi các quyết định thiết kế với `structure` hơn `convention`.
### G28: Đóng gói điều kiện (Encapsulate Conditionals)
### G29: Tránh câu điều kiện phủ định (Avoid Negative Conditionals)
- Khi có thể, các câu điều kiện nên được diễn đạt dưới dạng khẳng định.
### G30: Các chức năng nên làm một việc (Functions Should Do One Things)
- Việc tạo các hàm có nhiều phần thực hiện một loạt các thao tác thường rất hấp dẫn. Các chức năng loại này làm nhiều hơn một việc và nên được chuyển đổi thành nhiều chức năng nhỏ hơn, mỗi chức năng thực hiện một việc.
### G31: Khớp nối thời gian ẩn (Hidden Temporal Couplings)
- Khớp nối tạm thời thường là cần thiết, nhưng bạn không nên ẩn khớp nối.
### G32: Đừng Tùy Tiện (Don’t Be Arbitrary)
- Các lớp công khai không phải là tiện ích của một số lớp khác không nên nằm trong phạm vi của một lớp khác. Quy ước là đặt chúng ở chế độ công khai ở cấp cao nhất trong gói của chúng.
### G33: Đóng gói các điều kiện biên (Encapsulate Boundary Conditions)
- Các điều kiện biên rất khó theo dõi. Đặt xử lý cho chúng ở một nơi. đừng để chúng bị rò rỉ trên toàn bộ code.
### G34: Các chức năng chỉ nên giảm xuống một mức trừu tượng (Functions Should Descend Only One Level of Abstraction)
- Tất cả các câu lệnh trong một hàm đều phải được viết ở cùng một mức trừu tượng, mức này phải thấp hơn một mức so với thao tác được mô tả bằng tên của hàm.
- Tách các mức độ trừu tượng là một trong những chức năng quan trọng nhất của tái cấu trúc và là một trong những chức năng khó thực hiện tốt nhất.
### G35: Giữ dữ liệu có thể cấu hình ở mức cao (Keep Configurable Data at Hight Levels)
- Nếu bạn có một hằng số chẳng hạn như giá trị mặc định hoặc cấu hình đã biết và được mong đợi ở mức trừu tượng cao, đừng chôn nó trong một hàm cấp thấp. hiển thị nó dưới dạng đối số cho hàm cấp thấp được gọi từ hàm cấp cao.
### G36: Tránh điều hướng chuyển tiếp (Avoid Transitive Navigation)

## Java
### J1: Tránh danh sách nhập dài bằng cách sử dụng ký tự đại diện (Avoid Long Import Lists by Using Wildcards)
### J2: Không kế thừa các hằng số (Don’t Inherit Constants)
### J3: Hằng số so với Enums (Constants versus Enums)

## Names
### N1: Chọn Tên Mô Tả(Choose Descriptive Names)
- Hãy chắc chắn rằng tên là mô tả
### N2: Chọn tên ở mức độ trừu tượng phù hợp (Choose Names at the Appropriate Level of Abstraction)
- Đừng chọn những cái tên truyền đạt việc thực hiện; chọn tên phản ánh mức độ trừu tượng của lớp hoặc chức năng bạn đang làm việc.
### N3: Sử dụng danh pháp tiêu chuẩn nếu có thể (Use Standard Nomenclature Where Possible)
- Tên sẽ dễ hiểu hơn nếu chúng dựa trên quy ước hoặc cách sử dụng hiện có.
### N4: Tên rõ ràng (Unambiguous Names)
### N5: Sử dụng tên dài cho phạm vi dài (Use Long Names for Long Scopes)
- Độ dài của tên phải liên quan đến độ dài của phạm vi.
- Mặt khác, các biến và hàm có tên ngắn sẽ mất ý nghĩa trong một khoảng cách dài.
### N6: Tránh mã hóa (Avoid Encodings)
- Các tiền tố như `m_` hoặc `f` là vô dụng trong môi trường ngày nay.
- Một lần nữa, môi trường ngày nay cung cấp tất cả thông tin đó mà không cần phải xáo trộn tên.
### N7: Tên nên mô tả tác dụng phụ ( Names Should Describe Side-Effects)
- Không sử dụng một động từ đơn giản để mô tả một chức năng không chỉ là hành động đơn giản đó.

## Tests
### T1: Test không đầy đủ (Insufficient Tests)
### T2: Sử dụng Công cụ Bảo hiểm! (Use a Coverage Tool!)
### T3: Đừng bỏ qua các bài Test tầm thường (Don’t Skip Trivial Tests)
- Chúng dễ viết và giá trị tài liệu của chúng cao hơn chi phí sản xuất chúng.
### T4:  Một bài Test bị bỏ qua là một câu hỏi về sự mơ hồ (An Ignored Test Is a Question about an Ambiguity)
### T5:  Test điều kiện biên (Test Boundary Conditions)
- Chúng ta thường hiểu đúng phần giữa của một thuật toán nhưng đánh giá sai các ranh giới
### T6: Test toàn diện các lỗi gần (Exhaustively Test Near Bugs)
- Lỗi có xu hướng tụ tập.
- Khi tìm thấy lỗi trong một chức năng, ta nên thực hiện Test toàn diện chức năng đó. Ta có thể thấy rằng lỗi không đơn độc.
### T7: Các kiểu thất bại đang bộc lộ (Patterns of Failure Are Revealing)
### T8: Các mẫu phạm vi Test có thể được tiết lộ (Test Coverage Patterns can Be Revealing)
- Nhìn vào code được thực thi hoặc không được thực thi bởi các bài Test vượt qua sẽ đưa ra manh mối về lý do tại sao các bài Test không thành công lại thất bại.
### T9: Các bài Test phải nhanh (Tests Should Be Fast)

## Conclusion
- Tính chuyên nghiệp và tay nghề thủ công đến từ các giá trị thúc đẩy kỷ luật.

