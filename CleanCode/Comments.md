# Comments

- Đừng dùng comment để làm màu cho code
	+ Code sáng nghĩa và rõ ràng với ít comment sẽ tuyệt vời hơn so với code tối nghĩa, phức tạp với nhiều comment.
	Thay vì dành thời gian để viết comment giải thích mớ code hỗ lốn đó, hãy dành thời gian để dọn dẹp nó.
- Giải thích ý nghĩa ngay trong code
	+ Tạo ra một hàm có tên giống với comment mà bạn muốn viết
- Good Comments
	+ Comment thật sự tốt là comment không cần phải viết ra.
	+ Comment pháp lý
	+ Comment cung cấp thông tin
	+ Giải thích mục đích
	+ Làm dễ hiểu
		=> Trước khi bạn viết những comment như thế này, hãy chắc chắn rằng không còn cách nào tối ưu hơn, vì sau đó bạn cần quan tâm đến độ chính xác của chúng mỗi khi bạn chỉnh sửa lại code.
	+ Các cảnh báo về hậu quả
	+ TODO comments
	+ Khuếch đại: Comment có thể được dùng để khuếch đại tầm quan trọng của một cái gì đó có vẻ không quan trọng
	+ Javadocs in Public APIs
- Bad Comments
	+ Độc thoại
	+ Các comment thừa thải
	+ Các comment sai sự thật
	+ Các comment bắt buộc
	+ Các comment nhật ký
	+ Các comment gây nhiễu
	+ Đừng dùng comment khi bạn có thể dùng hàm hoặc biến
	+ Đánh dấu lãnh thổ
	+ Các comment kết thúc
	+ Thuộc tính và dòng tác giả
	+ Comment-hóa code
	+ HTML comment
	+ Thông tin phi tập trung
	+ Quá nhiều thông tin
	+ Mối quan hệ khó hiểu
	+ Comment làm tiêu đề cho hàm
	+ Javadocs trong code không công khai
