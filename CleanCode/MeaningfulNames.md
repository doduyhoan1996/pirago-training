# Meaningful Names

- Dùng những tên thể hiện được mục đích:
	+ Tên của biến, hàm, hoặc lớp phải trả lời tất cả những câu hỏi về nó. Nó phải cho bạn biết lý do nó tồn tại, nó làm được những gì, và dùng nó ra sao.
- Tránh sai lệch thông tin:
	+ Tránh để lại những dấu hiệu làm code trở nên khó hiểu. Tránh dùng những từ mang nghĩa khác với nghĩa cố định của nó.
	+ Không nên quy kết một nhóm là `List` nếu nó thực sự không phải là `List`.
	+ Cẩn thận với những cái tên gần giống nhau.
	+ Những cái tên không rõ ràng là ký tự L viết thường và O viết hoa. (Tránh sử dụng vì dễ gây hiểu nhầm)
- Tạo nên những sự khác biệt có nghĩa
- Dùng những tên phát âm được
- Dùng những tên tìm kiếm được
- Tránh việc mã hóa
	+ Ký pháp Hungary
	+ Các thành phần tiền tố
	+ Giao diện và thực tiễn
- Tránh “hiếp râm não” người khác
- Tên lớp: Tên lớp và các đối tượng nên sử dụng danh từ hoặc cụm danh từ. Tên lớp không nên dùng động từ.
- Tên các phương thức:
	+ Các phương thức nên có động từ hoặc cụm động từ.
	+ Các phương thức truy cập, chỉnh sửa thuộc tính phải được đặt tên cùng với `get`, `set` và `is` theo tiêu chuẩn của `Javabean`.
- Đừng thể hiện rằng bạn cute
	+ Chọn sự rõ ràng thay vì giải trí.
	+ Sự cute thường xuất hiện dưới dạng phong tục hoặc tiếng lóng, đừng mang những câu đùa trong văn hóa nước mình vào code
- Chọn một từ cho mỗi khái niệm
- Đừng chơi chữ
	+ Tránh dùng cùng một từ cho hai mục đích. Sử dụng cùng một thuật ngữ cho hai ý tưởng khác nhau về cơ bản là một cách chơi chữ.
- Dùng thuật ngữ
	+ Sử dụng các thuật ngữ khoa học, các thuật toán, tên mẫu (pattern),…cho việc đặt tên.
	+ Không khôn ngoan khi bạn đặt tên của vấn đề theo cách mà khách hàng định nghĩa
- Thêm ngữ cảnh thích hợp
