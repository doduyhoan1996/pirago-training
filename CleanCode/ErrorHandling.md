# Error Handling

- Sử dụng `Exceptions` hơn là trả về giá trị trong code
- Viết các khối `Try-Catch-Finally` của bạn trước
	+ Trong cách này, khối try giống như một transactions (phiên giao dịch). Catch của bạn sẽ không  được thực thi nếu không có vấn đề gì với try.
	+ Use Unchecked Exceptions (Sử dụng ngoại lệ không được kiểm tra)
	+ Định nghĩa lớp Exception theo nhu cầu của người gọi
		=> checked exceptions đôi khi có thể hữu ích nếu bạn đang viết thư viện quan trọng: Bạn phải catch chúng. Nhưng trong phát triển ứng dụng nói chung, chi phí phụ thuộc lớn hơn lợi ích mà nó đem lại.
- Provide Context with Exceptions (Cung cấp bối cảnh cho ngoại lệ)
	+ Mỗi ngoại lệ mà bạn đưa ra phải cung cấp đủ ngữ cảnh để xác định nguồn và vị trí của lỗi
	+ Tạo một thông điệp báo lỗi và truyền cho chúng với ngoại lệ của bạn. Đề cập đến các hành động gây ra lỗi và loại lỗi.
- Define Exception Classes in Terms of a Caller's Needs (Xác định các lớp ngoại lệ theo nhu cầu của người gọi)
- Define the Normal Flow: Định nghĩa một luồng bình thường (khi ngoại lệ lẫn lộn với code logic)
- Đừng trả về null
- Đừng truyền vào null

*Tóm lại:
- Sử dụng các exception thay vì trả về giá trị để các lớp gọi nó đỡ phải xử lý tiếp.
- Viết các đoạn code thành `Try-Catch-Finally` để dễ quan sát các xử lý code và ngoại lệ.
- Cung cấp đầy đủ thông tin ngoại lệ nhất (có thể ghi log) để dễ dàng debug.
- Nếu exception có code logic hoặc các exception gây ra bởi bên thứ 3, cân nhắc "wrapping"- đóng gọi lại trong một lớp ngoại lệ mới ta xây dựng.
- Chưa có cách hoàn hảo nhất để xử lý với `null`, hạn chế truyền và nhận nó vào các hàm.
