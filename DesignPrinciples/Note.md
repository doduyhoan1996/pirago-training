# Readable Code

- [Đọc tài liệu trong link tham khảo](https://docs.google.com/document/d/1pTx6D7yHaub-tQ5pmrCFZ4sfayURViSxy-T7vLdeZtM/edit)

# DRY principle (Don’t Repeat Yourself)

- Đừng lặp lại chính mình

# KISS principle (Keep it Simple, Stupid)

- Giữ cho mọi thứ đơn giản dễ hiểu (ngu ngốc)

# SOLID principle

## SOLID bao gồm 5 nguyên lý dưới đây:

- [Single Responsibility Principle](https://toidicodedao.com/2016/05/03/series-solid-cho-thanh-nien-code-cung-single-responsibility-principle/)
- [Open/Closed Principle](https://toidicodedao.com/2016/05/10/series-solid-cho-thanh-nien-code-cung-openclosed-principle/)
- [Liskov Substitution Principle](https://toidicodedao.com/2016/05/17/series-solid-cho-thanh-nien-code-cung-liskov-substitution-principle/)
- [Interface Segregation Principle](https://toidicodedao.com/2016/06/09/series-solid-cho-thanh-nien-code-cung-interface-segregation-principle/)
- [Dependency Inversion Principle](https://toidicodedao.com/2016/06/14/series-solid-cho-thanh-nien-code-cung-dependency-inversion-principle/)

### Single Responsibility Principle – Nguyên lý Đơn Trách Nhiệm

> Một class chỉ nên giữ một trách nhiệm duy nhất (Chỉ có thể thay đổi class vì một lý do duy nhất)

### Open/Closed Principle – Nguyên lý Đóng Mở

> Có thể thoải mái mở rộng 1 module, nhưng hạn chế sửa đổi bên trong module đó (open for extension but closed for modification).

### Liskov Substitution Principle - Nguyên lý Thay Thế Lít Kốp (LSP)

> Trong một chương trình, các object của class con có thể thay thế class cha mà không làm thay đổi tính đúng đắn của chương trình

### Interface Segregation Principle - Nguyên lý phân tách interface

> Thay vì dùng 1 interface lớn, ta nên tách thành nhiều interface nhỏ, với nhiều mục đích cụ thể

### Dependency Inversion Principle – Nguyên lý Đảo Ngược Dependency

> 1. Các module cấp cao không nên phụ thuộc vào các module cấp thấp. Cả 2 nên phụ thuộc vào abstraction.
> 2. Interface (abstraction) không nên phụ thuộc vào chi tiết, mà ngược lại. (Các class giao tiếp với nhau thông qua interface, không phải thông qua implementation.)
