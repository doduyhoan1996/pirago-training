# Pirago-Training

## CleanCode 
- Nắm rõ các quy tắc được giới thiệu trong sách Clean Code

## Git
- Sử dụng thành tạo các command của git + git flow

## PHP Standards Recommendations
- Hiểu và ghi nhớ các chuẩn viết code php được khuyên dùng trong tài liệu

## JAVASCRIPT BASE
- Nắm rõ kiến thức cơ bản về javascript

## Design Pattern
- Nắm rõ cách sử dụng, ứng dụng 6 mẫu pattern cơ bản

## PHP BASE
- Nẵm rõ kiến thức cơ bản về php

## Design Principles
- Nắm rõ các nguyên tắc để code clear + dễ truyền đạt thông tin hơn
- Nắm rõ các nguyên lý: DRY, KISS, SOLID, High cohesion and low coupling (Design princples)
