# PSR-1: Basic Coding Standard

## 1. Tổng Quan

- Các file PHẢI dùng các thẻ `<?php` và `<?=`.
- Các file PHẢI dùng duy nhất UTF-8 không BOM cho code PHP .
- Các file NÊN hoặc định nghĩa kí hiệu (lớp, hàm, hằng số, vv.) hoặc đưa ra tác dụng (e.g. sinh ra output, thay đổi .ini settings, vv.) nhưng KHÔNG NÊN làm cả 2.
- Các namespace và các class PHẢI tuân theo các chuẩn "autoloading" PSR: **[PSR-0, PSR-4]**.
- Tên class PHẢI được định nghĩa ở dạng `StudlyCaps`.
- Hằng số của phải PHẢI được định nghĩa bằng chữ cái in hoa và cách nhau bằng dấu gạch dưới.
- Các phương thức PHẢI đặt tên dạng `camelCase`.

## 2. Files

### 2.1. Thẻ PHP

- Code PHP PHẢI sử dụng kiểu mở thẻ đầy đủ để nhúng mã `<?php ?> ` hoặc short-echo `<?= ?> `; KHÔNG ĐƯỢC dùng các thẻ khác(tuy nhiên 1 số framework tuy có các thẻ khác nhưng khi biên dịch đều quay về đúng thẻ chuẩn).

### 2.2. Encoding

- File code PHP PHẢI dùng duy nhất UTF-8 không BOM.

### 2.3 Side Effects

- Một file NÊN khai báo các ký hiệu mới (lớp, hàm, hằng số, v.v.) và không gây ra tác dụng phụ nào khác hoặc nó NÊN thực thi logic với các tác dụng phụ, nhưng KHÔNG NÊN thực hiện cả hai.
- Cụm từ "side effects" có nghĩa là việc thực thi logic không liên quan trực tiếp đến việc khai báo các lớp, hàm, hằng số, v.v., chỉ đơn thuần là bao gồm file.
- "Side effects" bao gồm nhưng không giới hạn: tạo đầu ra, sử dụng rõ ràng yêu cầu hoặc bao gồm, kết nối với các dịch vụ bên ngoài, sửa đổi cài đặt ini, phát ra lỗi hoặc ngoại lệ, sửa đổi biến toàn cầu hoặc tĩnh, đọc hoặc ghi vào tệp và những thứ khác.

* Sau đây là một ví dụ về tệp có cả khai báo và tác dụng phụ; tức là, một ví dụ về những điều cần tránh:

```php
<?php
// side effect: change ini settings
ini_set('error_reporting', E_ALL);

// side effect: loads a file
include "file.php";

// side effect: generates output
echo "<html>\n";

// declaration
function foo()
{
    // function body
}
```

Còn đây là ví dụ NÊN làm theo:

```php
<?php
// declaration
function foo()
{
    // function body
}

// conditional declaration is *not* a side effect
if (! function_exists('bar')) {
    function bar()
    {
        // function body
    }
}
```

## 3. Các Namespace và tên class

- Namespace và Lớp PHẢI theo chuẩn "autoloading" PSR: **[PSR-0, PSR-4]**.
- Có nghĩa là mỗi lớp được khai báo trên mỗi file PHP riêng và namespace tối thiểu có một cấp, cấp đầu là tên vendor.
- Tên class PHẢI được định nghĩa ở dạng `StudlyCaps`.
- Với PHP 5.3 trở về sau, code PHẢI dùng namespace bình thường:
  **Ví dụ:**

```php
<?php
// PHP 5.3 and later:
namespace Vendor\Model;

class Foo
{
}
```

- Với các phiên bản 5.2.x trở về trước, NÊN đặt tên class với tên vendor đằng trước như sau:
  **Ví dụ:**

```php
<?php
// PHP 5.2.x and earlier:
class Vendor_Model_Foo
{
}
```

## 4. Các hằng số, thuộc tính và phương thức của lớp

### 4.1. Hằng

- Các hằng số lớp PHẢI được khai báo bằng tất cả chữ hoa với dấu phân cách gạch dưới. Ví dụ:

```php
<?php
namespace Vendor\Model;

class Foo
{
    const VERSION = '1.0';
    const DATE_APPROVED = '2012-06-01';
}
```

### 4.2. Thuộc tính

- Bạn có thể đặt ra quy ước đặt tên tùy ý, theo `$StudlyCaps`, `$camelCase`, hoặc `$under_score`
- Tuy nhiên, bất kỳ quy ước đặt tên nào được sử dụng NÊN được áp dụng nhất quán trong phạm vi hợp lý. Phạm vi đó có thể là cấp vendor, package, class hoặc method.

### 4.3. Phương thức

- Các phương thức PHẢI đặt tên dạng `camelCase`.

# Tham khảo

- https://www.php-fig.org/psr/psr-1/
