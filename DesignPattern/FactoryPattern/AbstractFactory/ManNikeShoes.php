<?php

class ManNikeShoes extends AbstractManShoes
{
    private $brand;
    private $color;
    function __construct()
    {
        $this->brand = 'Nike';
        $this->color = 'black';
    }
    function getBrand()
    {
        return $this->brand;
    }
    function getColor()
    {
        return $this->color;
    }
}
