<?php

/**
 * Abstract Object
 */
abstract class AbstractShoes
{
    abstract function getBrand();
    abstract function getColor();
}

abstract class AbstractWomanShoes extends AbstractShoes
{
    protected $bodyType = 'WomanShoes';
}
abstract class AbstractManShoes extends AbstractShoes
{
    protected $bodyType = 'ManShoes';
}
