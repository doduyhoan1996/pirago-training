# Giới thiệu
- **Abstract Factory (Kit)** là một design pattern thuộc nhóm Creational Pattern Design – những mẫu thiết kế cho việc khởi tạo đối tượng của lớp
- Được xây dựng dựa trên Factory Pattern và nó được xem là một factory cao nhất trong hệ thống phân cấp. Pattern này sẽ tạo ra các factory là class con của nó và các factory này được tạo ra giống như cách mà factory tạo ra các sub-class.
- **Mục đích:** Cung cấp một interface cho việc khởi tạo các tập hợp của những object có đặc điểm giống nhau mà không cần quan tâm object đó là gì.

# Ưu & nhược điểm
## Ưu điểm
- Có thể đảm bảo các đối tượng product nhận được từ factory sẽ tương thích với nhau.
- Tránh được những ràng buộc chặt chẽ giữa concrete products và client code.
- Nguyên tắc đơn lẻ: có thể trích xuất code tạo product vào một nơi và hỗ trợ code dễ dàng.
- Nguyên tắc mở/ đóng: có thể khởi tạo những bản mới của product mà không cần phá vỡ client code hiện có.
## Nhược điểm
- Code có thể trở nên phức tạp hơn mức bình thường, vì có rất nhiều interfaces và classes được khởi tạo cùng với mẫu.

# Khi nào thì sử dụng
**Sử dụng Abstract Facotry khi cần làm việc với các product có tính chất gần giống nhau và liên quan đến nhau và không cần phụ thuộc vào các định nghĩa có sẵn trong class đó:**
- Phía client sẽ không phụ thuộc vào việc những sản phẩm được tạo ra như thế nào.
- Ứng dụng sẽ được cấu hình với một hoặc nhiều họ sản phẩm.
- Các đối tượng cần phải được tạo ra như một tập hợp để có thể tương thích với nhau.
