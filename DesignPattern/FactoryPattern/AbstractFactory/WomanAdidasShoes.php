<?php 

class WomanAdidasShoes extends AbstractWomanShoes
{
    private $brand;
    private $color;
    function __construct()
    {
        $this->brand = 'Adidas';
        $this->color = 'white';
    }
    function getBrand()
    {
        return $this->brand;
    }
    function getColor()
    {
        return $this->color;
    }
}
