<?php

include_once('AbstractShoes.php');

/**
 * Abstract Factory
 */
abstract class AbstractShoesFactory
{
    abstract function makeWomanShoes();
    abstract function makeManShoes();
}


