<?php

class ManAdidasShoes extends AbstractManShoes
{
    private $brand;
    private $color;
    function __construct()
    {
        $this->brand = 'Adidas';
        $this->color = 'black';
    }
    function getBrand()
    {
        return $this->brand;
    }
    function getColor()
    {
        return $this->color;
    }
}
