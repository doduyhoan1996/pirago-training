<?php

include_once('NikeShoesFactory.php');
include_once('AdidasShoesFactory.php');

$nikeShoesFactory = new NikeShoesFactory;
$womanNikeShoes = $nikeShoesFactory->makeWomanShoes();
echo $womanNikeShoes->getBrand();
echo '<br />';
echo $womanNikeShoes->getColor();
echo '<br />';

$adidasShoesFactory = new AdidasShoesFactory;
$manAdidasShoes = $adidasShoesFactory->makeManShoes();
echo $manAdidasShoes->getBrand();
echo '<br />';
echo $manAdidasShoes->getColor();