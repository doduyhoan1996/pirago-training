<?php 

class WomanNikeShoes extends AbstractWomanShoes
{
    private $brand;
    private $color;
    function __construct()
    {
        $this->brand = 'Nike';
        $this->color = 'pink';
    }
    function getBrand()
    {
        return $this->brand;
    }
    function getColor()
    {
        return $this->color;
    }
}
