<?php

include_once('AbstractShoesFactory.php');
include_once('WomanNikeShoes.php');
include_once('ManNikeShoes.php');

class NikeShoesFactory extends AbstractShoesFactory
{
    private $manufacturer = 'Nike';
    function makeWomanShoes()
    {
        return new WomanNikeShoes;
    }
    function makeManShoes()
    {
        return new ManNikeShoes;
    }
}
