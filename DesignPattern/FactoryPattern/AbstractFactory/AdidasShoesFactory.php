<?php

include_once('AbstractShoesFactory.php');
include_once('WomanAdidasShoes.php');
include_once('ManAdidasShoes.php');

class AdidasShoesFactory extends AbstractShoesFactory
{
    private $manufacturer = 'Adidas';
    function makeWomanShoes()
    {
        return new WomanAdidasShoes;
    }
    function makeManShoes()
    {
        return new ManAdidasShoes;
    }
}
