<?php

include_once('Shoes.php');
include_once('Nike.php');
include_once('Adidas.php');
include_once('Puma.php');

class ShoesFactory
{
    public function getShoes($brand)
    {
        switch ($brand) {
            case Shoes::NIKE_BRAND;
                return new Nike;
                break;
            case Shoes::ADIDAS_BRAND:
                return new Adidas;
                break;

            case Shoes::PUMA_BRAND:
                return new Puma;
                break;
            default:
                return null;
                break;
        }
        return null;
    }
}
