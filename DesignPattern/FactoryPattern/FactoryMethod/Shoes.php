<?php

interface Shoes
{
    const NIKE_BRAND = 1;
    const ADIDAS_BRAND = 2;
    const PUMA_BRAND = 3;
    
    function getBrand();
}
