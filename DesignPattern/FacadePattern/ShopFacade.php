<?php

include_once('AccountService.php');
include_once('NotificationService.php');
include_once('PaymentService.php');
include_once('ShippingService.php');

class ShopFacade
{
    private static $instances;

    protected $accountService;
    protected $notificationService;
    protected $paymentService;
    protected $shippingService;

    /**
     * Facade constructor->
     *
     * @param AccountService|null $accountService
     * @param NotificationService|null $notificationService
     * @param PaymentService|null $subSystem2
     * @param ShippingService|null $shippingService
     */
    protected function __construct(
        AccountService $accountService = null,
        NotificationService $notificationService = null,
        PaymentService $paymentService = null,
        ShippingService $shippingService = null
    )
    {
        $this->accountService = $accountService ?: new AccountService();
        $this->notificationService = $notificationService ?: new NotificationService();
        $this->paymentService = $paymentService ?: new PaymentService();
        $this->shippingService = $shippingService ?: new ShippingService();
    }

    protected function __clone() { }

    public function __wakeup()
    {
        throw new \Exception("Cannot unserialize singleton");
    }

    public static function getInstance()
    {
        if (!isset(self::$instances)) {
            self::$instances = new self;
        }
        return self::$instances;
    }

    public function buyProductByCodWithFreeShipping()
    {
        $this->accountService->getAccout();
        $this->paymentService->paymentByCod();
        $this->shippingService->freeShipping();
        $this->notificationService->sendSms();
    }

    public function buyProductByPaypalWithStandardShipping()
    {
        $this->accountService->getAccout();
        $this->paymentService->paymentByPaypal();
        $this->shippingService->standardShipping();
        $this->notificationService->sendMail();
        $this->notificationService->sendSms();
    }

}

