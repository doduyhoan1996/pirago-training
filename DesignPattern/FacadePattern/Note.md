# Giới thiệu
- **Facade** là một mẫu thiết kế thuộc nhóm cấu trúc (Structural Pattern).
- **Facade Pattern** cung cấp cho chúng ta một giao diện chung đơn giản thay cho một nhóm các giao diện có trong một hệ thống con (subsystem). Facade Pattern định nghĩa một giao diện ở cấp độ cao hơn để giúp cho người dùng có thể dễ dàng sử dụng hệ thống con này.
- **Facade Pattern** cho phép các đối tượng truy cập trực tiếp giao diện chung này để giao tiếp với các giao diện có trong hệ thống con. Mục tiêu là che giấu các hoạt động phức tạp bên trong hệ thống con, làm cho hệ thống con dễ sử dụng hơn.

# Khi nào thì sử dụng
**Một số trường hợp mà khi gặp sẽ phải cân nhắc sử dụng Facade pattern:**
- *Muốn gom nhóm chức năng lại để Client dễ sử dụng.* Khi hệ thống có rất nhiều lớp làm người sử dụng rất khó để có thể hiểu được quy trình xử lý của chương trình. Và khi có rất nhiều hệ thống con mà mỗi hệ thống con đó lại có những giao diện riêng lẻ của nó nên rất khó cho việc sử dụng phối hợp. Khi đó có thể sử dụng Facade Pattern để tạo ra một giao diện đơn giản cho người sử dụng một hệ thống phức tạp.
- *Giảm sự phụ thuộc*. Khi bạn muốn phân lớp các hệ thống con. Dùng Façade Pattern để định nghĩa cổng giao tiếp chung cho mỗi hệ thống con, do đó giúp giảm sự phụ thuộc của các hệ thống con vì các hệ thống này chỉ giao tiếp với nhau thông qua các cổng giao diện chung đó.
- *Tăng khả năng độc lập và khả chuyển.*
- *Khi người sử dụng phụ thuộc nhiều vào các lớp cài đặt.* Việc áp dụng Façade Pattern sẽ tách biệt hệ thống con của người dùng và các hệ thống con khác, do đó tăng khả năng độc lập và khả chuyển của hệ thống con, dễ chuyển đổi nâng cấp trong tương lai.
- *Đóng gói nhiều chức năng, che giấu thuật toán phức tạp.*
- *Cần một interface không rắc rối mà dễ sử dụng.*

# Ưu & nhược điểm
## Ưu điểm
- Ta có thể tách mã nguồn của mình ra khỏi sự phức tạp của hệ thống con
- Hệ thống tích hợp thông qua Facade sẽ đơn giản hơn vì chỉ cần tương tác với Facade thay vì hàng loạt đối tượng khác.
- Tăng khả năng độc lập và khả chuyển, giảm sự phụ thuộc.
- Có thể đóng gói nhiều hàm được thiết kế không tốt bằng 1 hàm có thiết kế tốt hơn.
## Nhược điểm
- Class Facade của bạn có thể trở lên quá lớn, làm quá nhiều nhiệm vụ với nhiều hàm chức năng trong nó.
- Dễ bị phá vỡ các quy tắc trong SOLID.
- Việc sử dụng Facade cho các hệ thống đơn giản, ko quá phức tạp trở nên dư thừa.
