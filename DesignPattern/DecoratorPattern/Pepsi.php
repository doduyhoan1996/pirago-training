<?php

include_once('FoodDecorator.php');

class Pepsi extends FoodDecorator
{
    private const PRICE = 15;

    public function calculatePrice(): int
    {
        return $this->food->calculatePrice() + self::PRICE;
    }

    public function getDescription(): string
    {
        return $this->food->getDescription() . ', a Pepsi';
    }
}
