<?php

include_once('Burger.php');
include_once('Pepsi.php');
include_once('Chips.php');

$food = new Burger();

echo $food->getDescription();
echo PHP_EOL;
echo $food->calculatePrice();
echo '<br/>';
echo '-----------------------------'.PHP_EOL;
echo '<br/>';
$pepsi = new Pepsi($food);
echo $pepsi->getDescription();
echo PHP_EOL;
echo $pepsi->calculatePrice();
echo '<br/>';
echo '-----------------------------'.PHP_EOL;
echo '<br/>';
$chips = new Chips($pepsi);
echo $chips->getDescription();
echo PHP_EOL;
echo $chips->calculatePrice();
