<?php

include_once('Food.php');

class Burger implements Food
{
    public function calculatePrice()
    {
        return 40;
    }

    public function getDescription()
    {
        return 'A Burger';
    }
}
