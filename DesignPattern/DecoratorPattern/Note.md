# Decorator Pattern là gì?
- **Decorator Pattern** là 1 mẫu thiết kế cho phép chúng ta thêm các hành vi mới vào 1 đối tượng đã có. Việc này thực hiện bằng cách wrapper đối tượng đã có bằng 1 đối tượng mới. Thông qua đối tượng wrapper này chúng ta sẽ cung cấp thêm các hành vi mong muốn vào
- Hay nói cách khác, Decorator Pattern cho phép chúng ta thêm chức năng mới vào đối tượng đã có mà không ảnh hưởng đến các đối tượng khác (đã liên kết với đối tượng đã có).
- Mỗi khi cần thêm tính năng mới,chúng ta có thể đưa đối tượng hiện có wrap trong 1 đối tượng mới – gọi là Decorator Class.

# Kiến trúc
## Các thành phần trong mô hình:
- **Component:** là một interface quy định các method chung cần phải có cho tất cả các thành phần tham gia vào mẫu này.
- **Concrete Component:** là lớp hiện thực (implements) các phương thức của Component.
- **Decorator:** là một abstract class dùng để duy trì một tham chiếu của đối tượng Component và đồng thời cài đặt các phương thức của Component interface.
- **Concrete Decorator:** là lớp hiện thực (implements) các phương thức của Decorator, nó cài đặt thêm các tính năng mới cho Component.
- **Client:** đối tượng sử dụng Component với những yêu cầu mở rộng đính kèm

# Sử dụng Decorator Pattern khi nào?
- Sử dụng khi muốn thay đổi hành vi của 1 đối tượng trong lúc runtime mà không muốn chỉnh sửa đối tượng đó.
- Sử dụng trong trường hợp không thể sử dụng kế thừa vì 1 lý do nào đó, hoặc do mã nguồn sử dụng từ khóa final
- Trong 1 số trường hợp nếu viết theo hướng kế thừa thì mất quá nhiều công sức.

# Ưu & nhược điểm
## Ưu điểm
- Có thể mở rộng hành vi của đối tượng mà không cần tạo lớp con mới.
- Có thể thêm hoặc xoá tính năng của một đối tượng trong lúc thực thi.
- Một đối tượng có thể được bao bọc bởi nhiều wrapper cùng một lúc.
- Single Responsibility Principle - Có thể chia nhiều cách thực thi của một phương thức trong một lớp cho nhiều lớp nhỏ hơn.
## Nhược điểm
- Khó để xóa một wrapper cụ thể khỏi stack.
- Khó để triển khai decorator theo cách mà phương thức của nó không phụ thuộc vào thứ tự trong stack.                                                          |                