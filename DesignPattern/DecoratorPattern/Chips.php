<?php

// include_once('FoodDecorator.php');

class Chips extends FoodDecorator
{
    private const PRICE = 10;

    public function calculatePrice(): int
    {
        return $this->food->calculatePrice() + self::PRICE;
    }

    public function getDescription(): string
    {
        return $this->food->getDescription() . ', a chips';
    }
}
