<?php

abstract class FoodDecorator implements Food
{
    public function __construct(protected Food $food)
    {
    }
}
