<?php 

interface Food
{
    public function calculatePrice();
    public function getDescription();
}
