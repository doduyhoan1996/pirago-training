<?php

final class Database
{
    private static ?Database $obj = null;

    public static function getConnect(): Database
    {
        if (self::$obj === null) {
            self::$obj = new self();
        }

        return self::$obj;
    }

    private function __construct()
    {
        echo __CLASS__ . " initialize only once ";
    }

    private function __clone()
    {
    }

    public function __wakeup()
    {
        throw new Exception("Cannot unserialize database connection");
    }
}
