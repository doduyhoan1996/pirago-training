<?php

include_once('Invoker.php');
include_once('Calculator.php');
include_once('ConcreteCommand.php');

$number = new Invoker();
$calculator = new Calculator();
$command = new ConcreteCommand($calculator, '+', 6);
$number->Compute($command);
echo "Sau khi +6: " . $calculator->getCurrent() . "<br/>";
$command = new ConcreteCommand($calculator, '*', 5);
$number->Compute($command);
echo "Sau khi *5: " . $calculator->getCurrent() . "<br/>";
$command = new ConcreteCommand($calculator, '/', 2);
$number->Compute($command);
echo "Sau khi /3: " . $calculator->getCurrent() . "<br/>";
$command = new ConcreteCommand($calculator, '-', 3);
$number->Compute($command);
echo "Sau khi -3: " . $calculator->getCurrent() . "<br/>";
$number->Undo();
echo "Hoàn tác thao tác: " . $calculator->getCurrent() . "<br/>";
$number->Undo();
echo "Hoàn tác thao tác: " . $calculator->getCurrent() . "<br/>";
$number->Undo();
echo "Hoàn tác thao tác: " . $calculator->getCurrent() . "<br/>";
$number->Undo();
echo "Hoàn tác thao tác: " . $calculator->getCurrent() . "<br/>";
