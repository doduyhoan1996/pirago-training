<?php

include_once('Command.php');

class ConcreteCommand extends Command
{
    private $operator, $operand, $calculator;
    public function __construct($calculator, $operator, $operand)
    {
        $this->operator = $operator;
        $this->operand = $operand;
        $this->calculator = $calculator;
    }
    public function Execute()
    {
        $this->calculator->Action($this->operator, $this->operand);
    }
    public function unExecute()
    {
        $this->calculator->Action($this->Undo($this->operator), $this->operand);
    }
    private function Undo($operator)
    {
        switch ($operator) {
            case '+':
                return '-';
            case '-':
                return '+';
            case '*':
                return '/';
            case '/':
                return '*';
        }
    }
}
