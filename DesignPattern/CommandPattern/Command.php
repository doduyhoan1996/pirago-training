<?php

abstract class Command {
    abstract public function unExecute ();
    abstract public function Execute ();
}
