<?php 

class StudentsList
{
    private $_students = array();

    function __construct($list)
    {
        $this->_students = $list;
    }

    function sort(ISort $type)
    {
        $this->_students = $type->sort($this->_students);
    }

    function display()
    {
        echo '<ol>';
        foreach ($this->_students as $student) {
            echo "<li>{$student['last_name']} {$student['first_name']} : {$student['point']}</li>";
        }
        echo '</ol>';
    }
}
