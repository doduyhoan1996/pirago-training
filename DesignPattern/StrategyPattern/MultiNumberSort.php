<?php

class MultiNumberSort implements ISort
{
    private $_order;
    private $_index;

    function __construct($index, $order = 'ascending')
    {
        $this->_index = $index;
        $this->_order = $order;
    }

    function sort(array $list)
    {
        if ($this->_order == 'ascending') {
            uasort($list, array($this, 'ascSort'));
        } else {
            uasort($list, array($this, 'descSort'));
        }
        return $list;
    } 

    function ascSort($x, $y)
    {
        return ($x[$this->_index] > $y[$this->_index]);
    }
    function descSort($x, $y)
    {
        return ($x[$this->_index] < $y[$this->_index]);
    }
}