
<?php

include_once('StudentsList.php');
include_once('MultiAlphaSort.php');
include_once('MultiNumberSort.php');

$students = [
    256 => ['first_name' => 'Hoàng', 'last_name' => 'Nguyễn Đăng', 'point' => 96.5],
    2   => ['first_name' => 'Ánh', 'last_name' => 'Lê Xuân', 'point' => 78.1],
    9   => ['first_name' => 'Thọ', 'last_name' => 'Ngô Ngọc', 'point' => 94.0],
    364 => ['first_name' => 'Lộc', 'last_name' => 'Hoàng Văn', 'point' => 65.3],
    68  => ['first_name' => 'Phúc', 'last_name' => 'Trần Thanh', 'point' => 42.6]
];

$list = new StudentsList($students);

echo '<h2>Danh sách gốc</h2>';
$list->display();

$list->sort(new MultiAlphaSort('first_name'));
echo '<h2>Danh sách sắp xếp theo tên</h2>';
$list->display();

$list->sort(new MultiNumberSort('point', 'descending'));
echo '<h2>Danh sách sắp xếp theo điểm</h2>';
$list->display();

unset($list);
