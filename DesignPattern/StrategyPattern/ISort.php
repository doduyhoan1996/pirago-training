<?php

interface ISort
{
    function sort(array $list);
}