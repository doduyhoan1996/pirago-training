# Các toán tử trong javascript
## Có nhiều loại toán tử JavaScript khác nhau:
- toán tử số học *(Arithmetic Operators)*
- Toán tử gán *(Assignment Operators)*
- Toán tử so sánh *(Comparison Operators)*
- Toán tử logic *(Logical Operators)*
- Toán tử có điều kiện *(Conditional Operators)*
- Toán tử type *(Type Operators)*

# Các toán tử số học JS
- Toán tử số học được sử dụng để thực hiện số học trên các số:

| Operator    |	Name
| ----------- |	------------------
| +           |	Phép cộng
| -           |	phép trừ
| *           |	Phép nhân
| /           |	Phép chia
| %           |	Phép chia lấy dư.
| ++          |	Tăng thêm 1
| --          |	Giảm đi 1

# Toán tử gán JavaScript
- Toán tử gán được dùng để gán giá trị ở bên phải toán tử vào biến ở bên trái toán tử.

| Operator    |	Example            | Same As
| ----------- |	------------------ | ----------------
| = 	      |	x = y	           | x = y
| +=          |	x += y             | x = x + y
| -=          |	x -= y             | x = x - y
| *=          |	x *= y             | x = x * y
| /=          |	x /= y             | x = x / y
| %=          |	x %= y             | x = x % y
| **=	      | x **= y	           | x = x ** y

# Thêm chuỗi và số
- Cộng hai số sẽ trả về tổng, nhưng cộng một số và một chuỗi sẽ trả về một chuỗi:
**Example**
```
let x = 5 + 5;
let y = "5" + 5;
let z = "Hello" + 5;
```
**Kết quả của x, y và z sẽ là:**
```
10
55
Hello5
```

# Toán tử so sánh JavaScript

| Operator    |	Description
| ----------- |	---------------------
| ==          |	Bằng
| ===         |	Giống cả type
| !=          |	So sánh khác
| !==         |	khác hoặc khác type
| >           |	Lớn hơn
| <           | Nhỏ hơn
| >=          | Lớn hơn hoặc bằng
| <=          | Nhỏ hơn hoặc bằng
| ?           | toán tử bậc ba

# Toán tử logic JavaScript
| Operator    |	Description
| ----------- |	---------------------
| &&	      | logic và
| ||	      | logic hoặc
| !	          | logic không

# Toán tử Type JavaScript
| Operator    |	Description
| ----------- |	---------------------
| typeof	  | Trả về kiểu của một biến
| instanceof  | Trả về true nếu một đối tượng là một instance của một object type

# Toán tử Bitwise JavaScript
- Toán tử bit hoạt động trên các số 32 bit.
- Bất kỳ toán hạng số nào trong hoạt động đều được chuyển đổi thành số 32 bit. Kết quả được chuyển đổi lại thành một số JavaScript.

| Operator	| Description	                | Example	| Same as	      | Result	 | Decimal
| --------- | ------------------------------| --------- |---------------- | ---------|---------
| &	        | AND	                        | 5 & 1	    | 0101 & 0001	  | 0001	 | 1
| \|	    | OR	                        | 5 \| 1	| 0101 \| 0001	  | 0101	 | 5
| ~	        | NOT	                        | ~ 5	    | ~0101	          | 1010	 | 10
| ^	        | XOR	                        | 5 ^ 1	    | 0101 ^ 0001	  |0100	     | 4
| <<	    | dịch trái  		            | 5 << 1	| 0101 << 1	      | 1010     | 10
| >>	    | dịch chuyển phải	            | 5 >> 1	| 0101 >> 1	      | 0010     | 2
| >>>	    | dịch chuyển phải không dấu	| 5 >>> 1	| 0101 >>> 1	  | 0010	 | 2
