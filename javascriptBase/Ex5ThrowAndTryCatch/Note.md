# JavaScript Errors

# Throw, and Try...Catch...Finally

- Câu lệnh `try` định nghĩa một code block để chạy (để thử).
- Câu lệnh `catch` định nghĩa một code block để xử lý bất kỳ lỗi nào.
- Câu lệnh `finally` xác định một code block để chạy bất kể kết quả như thế nào.
- Câu lệnh `throw` xác định một lỗi tùy chỉnh.

# Errors Will Happen!

- Khi thực thi JavaScript code, có thể xảy ra các lỗi khác nhau.
- Lỗi có thể là lỗi mã hóa do lập trình viên tạo ra, lỗi do nhập sai và những điều không lường trước được khác.

# JavaScript try and catch

- Câu lệnh `try` cho phép ta xác định một code block sẽ được kiểm tra lỗi trong khi nó đang được thực thi.
- Câu lệnh `catch` cho phép ta xác định một code block sẽ được thực thi, nếu có lỗi xảy ra trong try block.
- Các câu lệnh JavaScript `try` and `catch` đi theo cặp:

```javascript
try {
  // Block of code to try
} catch (err) {
  // Block of code to handle errors
}
```

# JavaScript Throws Errors

- Khi xảy ra lỗi, JavaScript thường sẽ dừng và tạo thông báo lỗi.
- Thuật ngữ kỹ thuật cho điều này là: JavaScript sẽ đưa ra một ngoại lệ **(throw an error).**
  > JavaScript sẽ thực sự tạo một `Error object` với hai thuộc tính: `name` và `message`.

# The throw Statement

- Câu lệnh `throw` cho phép ta tạo một lỗi tùy chỉnh.
- Về mặt kỹ thuật, ta có thể đưa ra ngoại lệ **(throw an error).**.
- Ngoại lệ có thể là JavaScript `String`, một `Number`, một `Boolean` hoặc một `Object`
  > Nếu ta sử dụng `throw` cùng với `try` và `catch`, ta có thể kiểm soát luồng chương trình và tạo thông báo lỗi tùy chỉnh.

# HTML Validation

- Các trình duyệt hiện đại thường sẽ sử dụng kết hợp JavaScript và xác thực HTML tích hợp, sử dụng các quy tắc xác thực được xác định trước được xác định trong các thuộc tính HTML:

```javascript
<input id="demo" type="number" min="5" max="10" step="1">
```

# The finally Statement

- Câu lệnh finally cho phép ta thực thi code, sau khi `try` và `catch`, bất kể kết quả như thế nào:
  **Cú pháp**

```javascript
try {
  // Block of code to try
} catch (err) {
  // Block of code to handle errors
} finally {
  // Block of code to be executed regardless of the try / catch result
}
```
