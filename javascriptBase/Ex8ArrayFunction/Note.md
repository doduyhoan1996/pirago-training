# Các hàm sử dụng với mảng

## Các Hàm xử lý chuỗi hay sử dụng
- Hàm `push()`: chèn 1 phần tử vào cuối mảng
- Hàm `unshift()`: chèn phần từ vào đầu mảng
- Hàm `pop()`: xóa 1 phần tử khỏi cuối mảng
- Hàm `shift()`: xóa 1 phần tử khỏi đầu mảng
- Hàm `slice()`: Sử dụng copy mảng
- Hàm `Array.isArray()`: Kiểm tra có phải mang hay không?
- `length`: trả về kích cỡ của mảng

## Các phương thức quan trọng khác của Array

- .filter(): Lọc một mảng. Phương thức này trả lại một mảng, không làm thay đổi mạng gọi nó.
- forEach(): Duyệt qua từng phần tử của mảng
- find(): Giống như .filter(), nhưng hàm này chỉ trả lại 1 phần tử
- push(): Thêm 1 phần tử vào cuối mảng
- join(): Nối tất cả các phần tử của mảng thành một string
- concat(): Nối 2 hay nhiều mảng(hàm này sẽ trả lại một bản sao của mảng sau khi đã thực hiện nối, không làm thay đổi mảng gọi nó)