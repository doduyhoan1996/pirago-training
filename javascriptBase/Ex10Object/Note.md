# Object trong javascript

# Đối tượng / object trong javascript là gì ?

- Trong Javascript, object là một khái niệm trừu tượng, được dùng thể hiện cho các đối tượng mà qua đó ta có thể thêm các thuộc tính và phương thức cho đối tượng đó.

# Cú pháp khởi tạo đối tượng như sau:

- Có 2 cách để khởi tạo:
  - Cách 1: Sử dụng từ khóa `new Object()`
  ```javascript
  var Comment = new Object();
  ```
  - Cách 2: Sử dụng từ khóa {}
  ```javascript
  var Comment = {};
  ```
- Khi chúng ta khai báo một object thì tức là chúng ta đang tạo một instance của đối tượng Object, tức là đối tượng Object bản thân nó là một constructor. Vì vậy, ta không thể sử dụng từ khóa new để tạo ra một instance từ một instance được.

# Thuộc tính / phương thức của object trong javasript

- Mỗi đối tượng sẽ có các thuộc tính và phương thức.

## Thuộc tính

- Thuộc tính là những đặc điểm (có thể hiểu là biến) cần lưu trữ trong đối tượng.
- Có thể khai báo bằng ba cách:

  - Cách 1: Sử dụng từ khóa `new Object()`

  ```javascript
  // Khởi tạo
  var Comment = new Object();

  // Thêm thuộc tính
  Comment.title = "";
  Comment.content = "";
  Comment.fullname = "";
  Comment.email = "";
  ```

  - Cách 2: Sử dụng từ khóa {} và thêm thuộc tính ngay lúc khai báo

  ```javascript
  // Khởi tạo
  var Comment = {
    title: "",
    content: "",
    fullname: "",
    email: "",
  };
  ```

  - Cách 3: Sử dụng từ khóa {} và thêm thuộc tính sau đó

  ```javascript
  // Khởi tạo
  var Comment = {};

  // Thêm thuộc tính
  Comment.title = "";
  Comment.content = "";
  Comment.fullname = "";
  Comment.email = "";
  ```

  > Trong ba cách trên thì nên sử dụng cách thứ hai bởi vì nó mạch lạc và dễ quản lý code hơn.

## Phương thức

- Phương thức là những hành động (có thể hiểu là hàm) của đối tượng.
- Có ba cách khai báo:

  - Cách 1: Sử dụng từ khóa new Object()

  ```javascript
  // Khởi tạo
  var Comment = new Object();

  // Thêm phương thức
  Comment.addComment = function () {
    // do some thing
  };

  Comment.validateComment = function () {
    // do some thing
  };
  ```

  - Cách 2: Sử dụng từ khóa {} và thêm phương thức ngay lúc khai báo

  ```javascript
  // Khởi tạo
  var Comment = {
    addComment: function () {
      // do some thing
    },
    validateComment: function () {
      // do some thing
    },
  };
  ```

  - Cách 3: Sử dụng từ khóa {} và thêm phương thức sau đó

  ```javascript
  // Khởi tạo
  var Comment = {};

  // Thêm phương thức
  Comment.addComment = function () {
    // do some thing
  };

  Comment.validateComment = function () {
    // do some thing
  };
  ```

  > khuyến khích các bạn nên sử dụng cách 2.

## Xem danh sách phương thức và thuộc tính

- Cách nhanh nhất để xem và `debug` đối tượng thì bạn nên sử dụng [Firebug](https://freetuts.net/su-dung-firebug-de-kiem-tra-loi-javascript-380.html).

# Thao tác với thuộc tính và phương thức của object trong js

- Sau khi tạo xong đối tượng thì ta có hai cách sử dụng căn bản, đó là gọi và gán dữ liệu cho thuộc tính và phương thức. Nhưng nếu xem xét mọi khía cạnh thì chúng ta có các thao tác thông thường như sau:

  - Gán giá trị cho thuộc tính
  - Lấy giá trị của thuộc tính
  - Gọi phương thức

## Gán giá trị cho thuộc tính

- Để gán giá trị cho thuộc tính chúng ta chỉ việc thực hiện bằng cách sử dụng toán tử = giống như cách gán giá trị cho biến.

```javascript
Comment.title = "Tiêu đề bình luận";
```

- Nhưng nếu bạn gọi từ một hàm trong đối tượng thì bạn có thể sử dụng từ khóa `this`.

```javascript
var Comment = {
  title: "",
  addComment: function () {
    this.title = "Tiêu đề bình luận";
  },
};
```

## Lấy giá trị của thuộc tính

- Để lấy giá trị thuộc tính thì ta làm tương tự như thao tác với biến.

```javascript
var title = Comment.title;
```

- Nếu gọi từ một hàm trong đối tượng thì bạn có thể sử dụng từ khóa `this`.

```javascript
var Comment = {
  title: "",
  addComment: function () {
    var title = this.title;
  },
};
```

## Xóa thuộc tính

- Nếu bạn muốn xóa thuộc tính nào đó thì sử dụng từ khóa delete.

```javascript
var Comment = {
  title: "",
  addComment: function () {
    var title = this.title;
  },
};

// Xóa thuộc tính title, lúc này Comment chỉ còn 1 phương thức addComment
delete Comment.title;
```

## Gọi phương thức

- Tương tự như thuộc tính chúng ta gọi bình thường.

```javascript
comment.addComment();
```

- Gọi trong hàm của đối tượng.

```javascript
var Comment = {
  title: "",
  addComment: function () {
    this.validateComment();
  },
  validateComment: function () {
    // do some thing
  },
};
```

# Mảng chứa object - object chứa object

- Mỗi đối tượng (object) trong Javascript có thể chứa các đối tượng khác, hoặc một mảng cũng có thể chứa các đối tượng.

## object chứa object

- Ví dụ 1: Gom các thuộc tính của Comment vào một đối tượng `Info`.

```javascript
var Comment = {
  info: {
    title: "",
    content: "",
    email: "",
    fullname: "",
  },
};
```

- Ví dụ 2: Gom các phương thức (hàm) của Comment vào đối tượng func.

```javascript
var Comment = {
  func: {
    addComment: function () {
      // Something
    },
    validateComment: function () {
      // Something
    },
  },
};
```

- Lúc này để truy xuất tới các thuộc tính và phương thức này ta chỉ việc sử dụng dấu chấm và bổ sung thêm một cấp nữa.

```javascript
Comment.info.title = "Comment tại freetuts.net";
Comment.func.addComment();
```

## Mảng chứa object

- Để gán giá trị là một đối tượng vào mảng cũng tương tự như gán các giá trị bình thường khác

```javascript
// Đối tượng Comment
var Comment = {
  title: "",
  content: "",
  email: "",
  fullname: "",
};

// Khởi tạo mảng
var Comments = [];

// Gán giá trị cho phần tử mảng
Comments[0] = Comment;

// Gọi tới thuộc tính
Comments[0].title = "Tiêu đề bình luận";
alert(Comments[0].title);
```

hoặc

```javascript
// Khởi tạo mảng
var Comment = [
  {
    title: "",
    content: "",
    email: "",
    fullname: "",
  },
];

// Sử dụng
Comment[0].title = "Tiêu đề bình luận";
alert(Comment[0].title);
```

# Lặp qua từng phần tử của object trong js

- Muốn lặp qua từng phần tử của object thì ta sử dụng vòng lặp for .. in.
  **Cú pháp**

```javascript
for (key in object) {
  // key chính là phần tử trong quá trình lặp
}
```

**Ví dụ**

```javascript
let user = {
  name: "John",
  age: 30,
  isAdmin: true,
};

for (let key in user) {
  console.log(`Key ${key} có giá trị là ${user[key]}`);
}

// Kết quả:
// Key name có giá trị là John
// newhtml.html:19 Key age có giá trị là 30
// newhtml.html:19 Key isAdmin có giá trị là true
```
