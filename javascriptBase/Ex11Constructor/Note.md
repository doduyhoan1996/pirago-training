# Cách sử dụng constructor

# JavaScript Class Syntax

- Sử dụng từ khóa `class` để tạo một lớp.
- Luôn thêm một phương thức có tên `constructor()`:

**Cú pháp:**

```javascript
class ClassName {
  constructor() { ... }
}
```

**Ví dụ:**

```javascript
class Car {
  constructor(name, year) {
    this.name = name;
    this.year = year;
  }
}
```

- Ví dụ trên tạo một lớp có tên là `Car`.
- Lớp có hai thuộc tính ban đầu là `name` và `year`.

> Một lớp JavaScript không phải là một `object`.
> Nó là một khuôn mẫu cho `JavaScript objects.`.

## Sử dụng một Class

- Khi bạn có một `class`, bạn có thể sử dụng `class` để tạo các `object`:

```javascript
let myCar1 = new Car("Ford", 2014);
let myCar2 = new Car("Audi", 2019);
```

> Phương thức khởi tạo được gọi tự động khi một đối tượng mới được tạo.

# The Constructor Method

- Constructor method là một special method:
  - Nó phải có tên chính xác là `Constructor`
  - Nó được thực thi tự động khi một đối tượng mới được tạo
  - Nó được sử dụng để khởi tạo các thuộc tính đối tượng

> Nếu bạn không định nghĩa một constructor method, JavaScript sẽ thêm một phương thức khởi dựng rỗng.

# Class Methods

- Các Class method được tạo với cú pháp giống như các object method.
- Sử dụng từ khóa `class` để tạo một lớp.
- Luôn thêm `constructor()` method.
- Sau đó có thêm các method bất kỳ

**Cú Pháp**

```javascript
class ClassName {
  constructor() { ... }
  method_1() { ... }
  method_2() { ... }
  method_3() { ... }
}
```

**Vd:**

```javascript
class Car {
  constructor(name, year) {
    this.name = name;
    this.year = year;
  }
  age(x) {
    return x - this.year;
  }
}

let date = new Date();
let year = date.getFullYear();

let myCar = new Car("Ford", 2014);
document.getElementById("demo").innerHTML =
  "My car is " + myCar.age(year) + " years old.";
```
