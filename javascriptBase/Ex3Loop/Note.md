# Các loại vòng lặp trong javascript
- Các vòng lặp rất hữu ích nếu ta muốn chạy đi chạy lại cùng một đoạn code với mỗi lần có một giá trị khác nhau.

# For loop
- Với vòng lặp for ta sẽ khởi tạo biến đếm, kiểm tra điều kiện và tăng hoặc giảm biến được thực hiện trên cùng một dòng, do đó khá dễ dàng cho những người mới tiếp cận để debug và cũng giảm khả năng sinh ra lỗi
  **Cú pháp**
```javascript
for (bắt đầu; điều kiện; bước nhảy){
   /*Block of code*/
}
```

# While loop
- Câu lệnh while trong javascript sẽ tạo ra vòng lặp để thực hiện khối mã bên trong {} khi điều kiện đúng.
  **Cú pháp**
```javascript
while(Điều Kiện){
/* Thực thi code khi điều kiện đúng */
}
```

# Do ... While
- **do-while** về cơ bản khá giống với **while**. Đối với **Do While** dù điều kiện lặp như thế nào thì đoạn code vẫn được chạy ít nhất 1 lần còn nếu điều kiện thỏa mãn thì sẽ tương tự như **While** : tạo ra thêm vòng lặp
  **Cú pháp**
```javascript
do{
    /*Thực thi Block of code*/
} while (Điều kiện);
```

# forEach()
- **forEach** sẽ lặp lại từng phần tử trong mảng theo thứ tự index và thực thi function được truyền vào. 
> Lưu ý rằng **forEach** sẽ không thực thi function đầu vào cho các phần tử không có giá trị
  **Cú pháp**
```javascript
arrayName.forEach(function(currentValue, index, array){
    /* function body*/
})
```
### Hàm được truyền vào **forEach** sẽ nhận tối đa 3 đối số đầu vào:
- **currentValue:** Giá trị đang được vòng lặp xử lý
- **index:** Chí số của giá trị (**currentValue**) trong mảng
- **array:** toàn bộ array đang gọi đến **forEach**
> Không nhất thiết phải truyền toàn bộ 3 đối số vào, chỉ truyền vào những đối số cần thiết.

# Map
- **map** sẽ tạo ra một mảng mới chứ không phải thực thi với mảng gọi đến nó như **forEach**.
  **Cú pháp**
```javascript
var newArray= oldArray.map(function (currentValue, index, array){
    //Return element for the newArray
});
```
### Tương tự như forEach, map cũng lấy 3 tham số đầu vào
- **currentValue:** Giá trị đang được vòng lặp xử lý
- **index:** Chí số của giá trị (**currentValue**) trong mảng
- **array:** toàn bộ array đang gọi đến **forEach**

# For...in
- **For ... in** mục đích chủ yếu được dùng để loop trong một **object** chứ không phải **array** . Số lượng vòng lặp sẽ tương ứng với số lượng thuộc tính của **object**.
- Cũng có thể sử dụng **for...in** cho **array**, tuy nhiên *key* sẽ tương ứng với giá trị *index* của từng phần tử
  **Cú pháp**
```javascript
for (variableName in object) {
    // Block of code
}
```
> for...in không được khuyến khích sử dụng với những mảng mà thứ tự index của nó quan trọng.

# for...of
- Vòng lắp được ra mắt trong phiên bản ES6. Hàm này có thể sử dụng để duyệt phần lớn các đối tượng từ *Array, String, Map, WeakMap, Set ,...*
  **Cú pháp**
```javascript
for (variable of iterable) {
    // Block of code
}
```
*Ví dụ*
```javascript
var str= 'paul';
for (var value of str) {
    console.log(value);
}
// "p"
// "a"
// "u"
// "l"

let itobj = new Map([['x', 0], ['y', 1], ['z', 2]]);
for (let kv of itobj) {
  console.log(kv);
}
// ['x', 0]
// ['y', 1]
// ['z', 2]

for (let [key, value] of itobj) {
  console.log(value);
}

//0
//1
//2
```
