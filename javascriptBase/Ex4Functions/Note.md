# Hàm trong javascript (JavaScript Function)
- **JavaScript Function** là một khối code được thiết kế để thực hiện một tác vụ cụ thể.
- Một **JavaScript Function** được thực thi khi "cái gì đó" gọi nó (calls it).

# Cú pháp **JavaScript Function**
- **JavaScript Function** được xác định bằng từ khóa `function`, theo sau là **name**, tiếp theo là dấu ngoặc đơn **()**.
- *Function names* có thể chứa các chữ cái, chữ số, dấu gạch dưới và ký hiệu đô la (quy tắc giống như biến).
- Dấu ngoặc đơn có thể bao gồm các tên tham số được phân tách bằng dấu phẩy:
**(parameter1, parameter2, ...)**
- Code được thực thi bởi function, được đặt bên trong dấu ngoặc nhọn: **{}**
```javascript
function name(parameter1, parameter2, parameter3) {
  // code to be executed
}
```
- Các tham số của hàm được liệt kê bên trong dấu ngoặc đơn **()** trong định nghĩa hàm.
- Các đối số của hàm là các giá trị mà hàm nhận được khi nó được gọi.
- Bên trong hàm, các đối số (các tham số) hoạt động như các biến cục bộ.

# Function calling (invoking)
### Code bên trong hàm sẽ thực thi khi "cái gì đó" invokes (calls) hàm:
- Khi một sự kiện xảy ra (khi người dùng nhấp vào nút)
- Khi nó được invoked (called) từ mã JavaScript
- Tự động (self invoked)

# Function Return
- Khi gặp lệnh `return`, hàm sẽ thoát và trả về giá trị nếu có.

# Why Functions?
- Có thể sử dụng lại mã: Xác định mã một lần và sử dụng nhiều lần.
- Có thể sử dụng cùng một mã nhiều lần với các đối số khác nhau để tạo ra các kết quả khác nhau.

# The () Operator Invokes the Function
- Truy cập một `function`  không có () sẽ trả về đối tượng `function`  thay vì kết quả `function`.

# Functions Sử dụng như Variable Values
- `Functions` có thể được sử dụng giống như cách bạn sử dụng các biến, trong tất cả các loại công thức, bài tập và phép tính.

# Biến cục bộ (Local Variables)
- Các biến được khai báo trong JavaScript function, trở thành LOCAL cho function.
- Local variables chỉ có thể được truy cập từ bên trong function.
- Vì các biến cục bộ chỉ được nhận dạng bên trong các hàm của chúng, nên các biến có cùng tên có thể được sử dụng trong các hàm khác nhau.
- Các biến cục bộ được tạo khi chức năng bắt đầu và bị xóa khi chức năng hoàn thành.


