# Các hàm sử dụng với chuỗi
## Các Hàm xử lý chuỗi hay sử dụng

- `toLowerCase()`: Đổi in thường
- `toUpperCase()`: Đổi in hoa
- `trim()`: Cắt các ký tự trắng 2 đầu chuỗi
- `length()`: Lấy độ dài chuỗi
- `substring()`: Lấy chuỗi con
- `charAt(index)`: Lấy ký tự tại vị trí
- `replaceAll(find, replace)`: Tìm kiếm và thay thế tất cả
- `Split(separator)`: Tách chuỗi thành mảng
- `equals()`: So sánh bằng có phân biệt hoa/thường
- `equalsIgnoreCase()`: So sánh không phân biệt hoa/thường
- `Contains():` Kiểm tra có chứa hay không
- `startsWith()`: Kiểm tra có bắt đầu bởi hay không
- `endsWith()`: Kiểm tra có kết thúc bởi hay không
- `matches()`: So khớp với hay không?
- `indexOf()`: Tìm vị trí xuất hiện đầu tiên của chuỗi con
- `lastIndexOf()`: Tìm vị trí xuất hiện cuối cùng của chuỗi con
- `slice() `: trích xuất một phần của chuỗi và trả về phần được trích xuất trong một chuỗi mới.
