# Các event trong javascript

# JavaScript Events

- Các `event HTML`  là "những thứ" xảy ra với các phần tử HTML.
- Khi `JavaScript` được sử dụng trong các trang HTML, `JavaScript` có thể "phản hồi" với những `event` này.

## HTML Events

- Một `event HTML`  có thể là điều mà trình duyệt thực hiện hoặc điều gì đó mà người dùng thực hiện.
- Một số ví dụ về các `event HTML` :
  - Một trang web HTML đã tải xong
  - Một trường đầu vào HTML đã được thay đổi
  - Một nút HTML đã được click vào

## Event HTML phổ biến

| Event       | Description                                     |
| ----------- | ----------------------------------------------- |
| onchange    | Một phần tử HTML đã được thay đổi               |
| onclick The | Người dùng nhấp vào một phần tử HTML            |
| onmouseover | Người dùng di chuyển chuột qua phần tử HTML     |
| onmouseout  | Người dùng di chuyển chuột ra khỏi phần tử HTML |
| onkeydown   | Người dùng nhấn một phím bàn phím               |
| onload      | Trình duyệt đã tải xong trang                   |

(xem thêm các event khác)[https://www.w3schools.com/jsref/dom_obj_event.asp]

## Trình xử lý `event` JavaScript
- Trình xử lý `event` có thể được sử dụng để xử lý và xác minh đầu vào của người dùng, hành động của người dùng và hành động của trình duyệt:
    - Những việc nên làm mỗi khi tải trang
    - Những việc nên làm khi đóng trang
    - Hành động nên được thực hiện khi người dùng nhấp vào nút
    - Nội dung cần được xác minh khi người dùng nhập dữ liệu
    - V.v ...
- Có thể sử dụng nhiều phương thức khác nhau để JavaScript hoạt động với các `event`:
    - Thuộc tính `event HTML`  có thể thực thi trực tiếp mã JavaScript
    - Thuộc tính `event HTML`  có thể gọi hàm JavaScript
    - Bạn có thể gán các chức năng xử lý `event` của riêng mình cho các phần tử HTML
    - Bạn có thể ngăn các `event` được gửi hoặc được xử lý
    - V.v ...

                                                                                                                                                                                                                                                                                                                                                                                                